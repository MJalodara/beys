<?php
// include 'config.php';
include 'db.php';
include 'functions.php';
db_connect();
function from_obj(&$type, $default = "")
{
    return isset($type) ? $type : $default;
}

$res = file_get_contents('php://input');
$json = json_decode($res);

$bets = from_obj($json->events, array());
$uid = from_obj($json->uid, "");
$response = array();

// SELECT b.uid,b.amount,bs.selection_id,bs.maxBetAmount,bs.id FROM `bets_tmp` b LEFT JOIN bet_slip_tmp bs ON b.id=bs.bet_id where b.uid='2' and bs.selection_id = '1357308801' ORDER BY bs.id DESC LIMIT 1

$isValid = 1;
foreach ($bets as $bet) {
    $isValid = $isValid + 1;
    // $event_id = $bet->event_id;
    $selection_id = from_obj($bet->selection_id, 0);
    $maxBet = from_obj($bet->maxBet, "0");
    $amount = from_obj($json->amount, "");

    // $betData = getRow("SELECT b.uid,b.amount,bs.selection_id,bs.maxBetAmount,bs.id FROM `bets_tmp` b LEFT JOIN bet_slip_tmp bs ON b.id=bs.bet_id where b.uid='2' and bs.selection_id = '1357308801' ORDER BY bs.id DESC LIMIT 1", array("selection_id" => $selection_id, "uid" => $uid));
    // $query = $con->prepare("SELECT b.uid,b.amount,bs.selection_id,bs.maxBetAmount,bs.id 
    //     FROM `bets_tmp` b LEFT JOIN bet_slip_tmp bs ON b.id=bs.bet_id
    //     where b.uid=:uid and bs.selection_id = :selection_id 
    //     ORDER BY bs.id DESC LIMIT 1");

    $query = $con->prepare("SELECT b.uid,SUM(b.amount) amount,bs.selection_id,bs.maxBetAmount 
        FROM `bets_tmp` b LEFT JOIN bet_slip_tmp bs ON b.id=bs.bet_id
        where b.uid=:uid and bs.selection_id = :selection_id
        -- GROUP BY bs.maxBetAmount
        ");
    $query->bindParam(":selection_id", $selection_id);
    $query->bindParam(":uid", $uid);

    $query->execute();
    $betData = array();
    if ($query->rowCount() > 0) {
        $betData = $query->fetch(PDO::FETCH_ASSOC);
        if (isset($betData['amount'])) {
            $x = $betData['amount'];
            $y = $maxBet;
            $mXY = $y - $x;
            if ($mXY <= $amount) {
                if (count($bets) == $isValid) {
                    $response['success'] = "OK";
                    $response['result'] = "OK";
                    $response['result_text'] = '';
                    $response['details'] = [];
                    echo json_encode($response);
                    exit;
                }
            } else {
                $response['success'] = "NO";
                $response['result'] = "OK";
                $response['result_text'] = 'Balance not valid...';
                $response['details'] = [];
                echo json_encode($response);
                exit;
            }
        }else{
            if (count($bets) == $isValid) {
                $response['success'] = "OK";
                $response['result'] = "OK";
                $response['result_text'] = '';
                $response['details'] = [];
                echo json_encode($response);
                exit;
            }
        }

    }
}

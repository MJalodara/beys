<?php
// include 'config.php';
include 'db.php';
include 'functions.php';
db_connect();

function from_obj(&$type,$default = "") {
    return isset($type)? $type : $default;
}

$res = file_get_contents('php://input');
$json = json_decode($res);

$uid = from_obj( $json->uid, "");

$response = array();

$query = $con->prepare ( "SELECT b.*,u.first_name,u.last_name,u.id userId,u.username FROM `buddy_transfer` b LEFT JOIN users u ON u.id=b.to_id WHERE b.`from_id`=:uid ");
$query->bindParam(":uid", $uid);


$query->execute ();
$batsData = array ();
if ($query->rowCount () > 0) {
$batsData= $query->fetchAll ( PDO::FETCH_ASSOC );

$jsonData['details'] = $batsData;
$response['success'] = "true";
$response['result'] = 0;
$response['data'] = $jsonData; 
echo json_encode($response);  
}else{
    $jsonData['details'] = [];
    $response['success'] = "true";
    $response['result'] = 0;
    $response['data'] = $jsonData;
    echo json_encode($response); 
}

-- phpMyAdmin SQL Dump
-- version 4.9.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Feb 09, 2020 at 08:46 AM
-- Server version: 8.0.18
-- PHP Version: 7.3.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `vbet`
--

-- --------------------------------------------------------

--
-- Table structure for table `bets_tmp`
--

CREATE TABLE `bets_tmp` (
  `id` int(11) NOT NULL,
  `uid` int(11) NOT NULL,
  `bat_slip_id` int(11) DEFAULT NULL,
  `type` int(1) NOT NULL,
  `odd_type` int(1) NOT NULL,
  `amount` decimal(10,2) NOT NULL DEFAULT '0.00',
  `balanace_after` varchar(10) NOT NULL,
  `balance_after_win` varchar(10) NOT NULL,
  `k` decimal(10,2) NOT NULL DEFAULT '0.00',
  `currency` varchar(6) NOT NULL,
  `outcome` int(11) NOT NULL DEFAULT '0',
  `number` text CHARACTER SET utf8 COLLATE utf8_general_ci,
  `client_id` int(11) DEFAULT NULL,
  `betshop_id` text CHARACTER SET utf8 COLLATE utf8_general_ci,
  `is_live` text CHARACTER SET utf8 COLLATE utf8_general_ci,
  `payout` int(11) NOT NULL,
  `possible_win` int(11) NOT NULL,
  `accept_type_id` int(11) NOT NULL,
  `system_min_count` text NOT NULL,
  `client_login` varchar(30) NOT NULL,
  `barcode` int(11) NOT NULL,
  `calc_date` int(11) NOT NULL,
  `date_time` int(11) NOT NULL,
  `client_bonus_id` text NOT NULL,
  `bonus_bet_amount` decimal(10,2) NOT NULL DEFAULT '0.00',
  `bonus` decimal(10,2) NOT NULL DEFAULT '0.00',
  `is_super_bet` text NOT NULL,
  `is_bonus_money` text NOT NULL,
  `source` int(11) NOT NULL,
  `draw_number` text NOT NULL,
  `tags` text NOT NULL,
  `old_bet_id` text NOT NULL,
  `auto_cash_out_amount` text NOT NULL,
  `additional_amount` text NOT NULL,
  `each_way` text NOT NULL,
  `total_partial_cashout_amount` text NOT NULL,
  `remaining_stake` decimal(10,2) NOT NULL DEFAULT '0.00',
  `tax_amount` decimal(10,2) NOT NULL DEFAULT '0.00',
  `additional_info` text NOT NULL,
  `has_recalculation_reason` text NOT NULL,
  `info_cashdesk_id` text NOT NULL,
  `stake_tax_amount` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `bets_tmp`
--

INSERT INTO `bets_tmp` (`id`, `uid`, `bat_slip_id`, `type`, `odd_type`, `amount`, `k`, `currency`, `outcome`, `number`, `client_id`, `betshop_id`, `is_live`, `payout`, `possible_win`, `accept_type_id`, `system_min_count`, `client_login`, `barcode`, `calc_date`, `date_time`, `client_bonus_id`, `bonus_bet_amount`, `bonus`, `is_super_bet`, `is_bonus_money`, `source`, `draw_number`, `tags`, `old_bet_id`, `auto_cash_out_amount`, `additional_amount`, `each_way`, `total_partial_cashout_amount`, `remaining_stake`, `tax_amount`, `additional_info`, `has_recalculation_reason`, `info_cashdesk_id`, `stake_tax_amount`) VALUES
(1, 1, 0, 1, 0, '11.00', '3.55', 'INR', 0, '', 0, '', '1', 47, 47, 0, 'undeknown', '8128500518', 0, 1580817553, 1580817553, 'undeknown', '0.00', '0.00', '', '', 42, 'undeknown', 'undeknown', '11', 'undeknown', 'undeknown', '', 'undeknown', '0.00', '0.00', 'undeknown', '', 'undeknown', 'undeknown'),
(2, 1, 0, 1, 0, '2.00', '4.20', 'INR', 0, '', 0, '', '1', 47, 47, 0, 'undeknown', '8128500518', 0, 1580817553, 1580817553, 'undeknown', '0.00', '0.00', '', '', 42, 'undeknown', 'undeknown', '2', 'undeknown', 'undeknown', '', 'undeknown', '0.00', '0.00', 'undeknown', '', 'undeknown', 'undeknown'),
(3, 1, 0, 2, 0, '1.00', '70.40', 'INR', 0, '', 0, '', '1', 70, 70, 0, 'undeknown', 'mahesh', 0, 1580921824, 1580921824, 'undeknown', '0.00', '0.00', '', '', 42, 'undeknown', 'undeknown', '1', 'undeknown', 'undeknown', '', 'undeknown', '1.00', '0.00', 'undeknown', '', 'undeknown', 'undeknown'),
(4, 1, 0, 1, 0, '1.00', '2.10', 'INR', 0, '', 0, '', '1', 2, 2, 0, 'undeknown', 'mahesh', 0, 1580924350, 1580924350, 'undeknown', '0.00', '0.00', '', '', 42, 'undeknown', 'undeknown', '1', 'undeknown', 'undeknown', '', 'undeknown', '1.00', '0.00', 'undeknown', '', 'undeknown', 'undeknown'),
(5, 1, 0, 1, 0, '10.00', '4.25', 'INR', 0, '', 0, '', '1', 43, 43, 0, 'undeknown', 'mahesh', 0, 1580925009, 1580925009, 'undeknown', '0.00', '0.00', '', '', 42, 'undeknown', 'undeknown', '10', 'undeknown', 'undeknown', '', 'undeknown', '10.00', '0.00', 'undeknown', '', 'undeknown', 'undeknown'),
(6, 1, NULL, 1, 0, '1.00', '1.75', 'INR', 0, NULL, NULL, NULL, '1', 2, 2, 0, 'undeknown', 'mahesh', 0, 1581237977, 1581237977, 'undeknown', '0.00', '0.00', '', '', 42, 'undeknown', 'undeknown', '1', 'undeknown', 'undeknown', '', 'undeknown', '1.00', '0.00', 'undeknown', '', 'undeknown', 'undeknown');

-- --------------------------------------------------------

--
-- Table structure for table `bet_slip_tmp`
--

CREATE TABLE `bet_slip_tmp` (
  `bet_id` int(11) NOT NULL,
  `selection_id` int(11) NOT NULL,
  `coeficient` decimal(10,2) NOT NULL DEFAULT '0.00',
  `outcome` int(11) NOT NULL DEFAULT '0',
  `outcome_name` varchar(20) NOT NULL,
  `game_info` varchar(20) NOT NULL,
  `event_name` varchar(20) NOT NULL,
  `game_start_date` int(11) NOT NULL,
  `team1` varchar(100) NOT NULL,
  `team2` varchar(100) NOT NULL,
  `competition_name` varchar(100) NOT NULL,
  `game_id` int(11) NOT NULL,
  `is_live` text NOT NULL,
  `sport_id` int(11) NOT NULL,
  `sport_name` varchar(40) NOT NULL,
  `sport_index` varchar(40) NOT NULL,
  `region_name` varchar(40) NOT NULL,
  `market_name` varchar(40) NOT NULL,
  `match_display_id` int(11) NOT NULL,
  `game_name` text NOT NULL,
  `basis` varchar(100) NOT NULL DEFAULT '',
  `match_info` text NOT NULL,
  `exact_name` varchar(100) NOT NULL,
  `info` text NOT NULL,
  `home_score` text NOT NULL,
  `away_score` text NOT NULL,
  `cash_out_price` text NOT NULL,
  `selection_price` decimal(10,2) NOT NULL DEFAULT '0.00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `bet_slip_tmp`
--

INSERT INTO `bet_slip_tmp` (`bet_id`, `selection_id`, `coeficient`, `outcome`, `outcome_name`, `game_info`, `event_name`, `game_start_date`, `team1`, `team2`, `competition_name`, `game_id`, `is_live`, `sport_id`, `sport_name`, `sport_index`, `region_name`, `market_name`, `match_display_id`, `game_name`, `basis`, `match_info`, `exact_name`, `info`, `home_score`, `away_score`, `cash_out_price`, `selection_price`) VALUES
(1, 1273108644, '3.55', 0, 'NotResulted', 'X', 'X', 1580814000, 'SCG Muangthong United', 'Port FC', 'Draw', 16021912, '1', 1, 'Soccer', '1', 'World', 'Match Result', 15952435, 'SCG Muangthong United - Port FC', '0.00', '1 : 0, (1:0)', '', 'undeknown', '1', '0', 'undeknown', '3.55'),
(2, 1273231403, '4.20', 0, 'NotResulted', 'X', 'X', 1580815800, 'KF Drenasi', 'Dukagjini FK', 'Draw', 16022859, '1', 1, 'Soccer', '1', 'World', 'Match Result', 15952435, 'KF Drenasi - Dukagjini FK', '0.00', '1 : 0, (1:0) 15`', '', 'undeknown', '1', '0', 'undeknown', '4.20'),
(3, 1274305128, '11.00', 0, 'NotResulted', 'W1', 'P1', 1580918400, 'FC Nordsjaelland', 'CSKA Moscow', 'FC Nordsjaelland ', 16020262, '1', 1, 'Soccer', '1', 'World', 'Match Result', 15952435, 'FC Nordsjaelland - CSKA Moscow', 'undeknown', '0 : 1, (0:1)', '', 'undeknown', '0', '1', 'undeknown', '11.00'),
(3, 1273490428, '6.40', 0, 'NotResulted', 'X', 'X', 1580918400, 'Polonia Bytom', 'BKS Sarmacja Bedzin', 'Draw', 16023727, '1', 1, 'Soccer', '1', 'World', 'Match Result', 15952435, 'Polonia Bytom - BKS Sarmacja Bedzin', 'undeknown', '1 : 0, (1:0), (0:0) 48`', '', 'undeknown', '1', '0', 'undeknown', '6.40'),
(4, 1275099708, '2.10', 0, 'NotResulted', 'Home', 'Home', 1580918400, 'FC Nordsjaelland', 'CSKA Moscow', 'FC Nordsjaelland ', 16020262, '1', 1, 'Soccer', '1', 'World', 'Goals Handicap', 15952435, 'FC Nordsjaelland - CSKA Moscow', '2 / 2', '0 : 2, (0:1), (0:1) 87`', '', 'undeknown', '0', '2', 'undeknown', '2.10'),
(5, 1228470400, '4.25', 0, 'NotResulted', 'Home', 'Home', 1580923800, 'Bayer Leverkusen', 'VfB Stuttgart', 'Bayer Leverkusen ', 15846489, '1', 1, 'Soccer', '1', 'World', 'Goals Handicap', 15952435, 'Bayer Leverkusen - VfB Stuttgart', '-2.5', '0 : 0, (0:0) 20`', '', 'undeknown', '0', '0', 'undeknown', '4.25'),
(6, 1280524600, '1.75', 0, 'NotResulted', 'X', 'X', 1581232800, 'Shakhtar Donetsk', 'FK Ventspils', 'X', 16036484, '1', 1, 'Soccer', '1', 'World', 'Rest Of The Match (Current Score: 1-0)', 15952435, 'Shakhtar Donetsk - FK Ventspils', '', '1 : 0, (1:0), (0:0) 77`', 'X', 'undeknown', '1', '0', 'undeknown', '1.75');

-- --------------------------------------------------------

--
-- Table structure for table `currency`
--

CREATE TABLE `currency` (
  `id` int(11) NOT NULL,
  `currency_id` varchar(150) NOT NULL,
  `currency_name` varchar(150) NOT NULL,
  `currency` varchar(150) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `currency`
--

INSERT INTO `currency` (`id`, `currency_id`, `currency_name`, `currency`) VALUES
(1, 'INR', 'Indian rupee ', 'INR');

-- --------------------------------------------------------

--
-- Table structure for table `results`
--

CREATE TABLE `results` (
  `id` int(11) NOT NULL,
  `game_id` varchar(20) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `sport_id` int(11) NOT NULL,
  `json_lines` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_bin
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `results`
--

INSERT INTO `results` (`id`, `game_id`, `sport_id`, `json_lines`) VALUES
(1, '15990221', 1, '{\"sport_id\":1,\"game_id\":15990221,\"tableData\":[{\"column_name\":\"Total_Goals_Asian\",\"value\":\"Over (0.75),Over (1.25),Over (1.75),Under (1.75),Under (2.25),Over (2.25),Under (2.75),Under (3.25),Under (3.75)\"},{\"column_name\":\"2nd_Half_Total_Goals_Odd_Even\",\"value\":\"Even\"},{\"column_name\":\"Total_Goals_3_Way\",\"value\":\"Over (1),Exactly (2),Under (3)\"},{\"column_name\":\"1st_Half_Total_Goals_3_Way\",\"value\":\"Under (1),Under (2)\"},{\"column_name\":\"1st_Half_Goals_Asian_Handicap\",\"value\":\"Madura United FC (-0.25),Terengganu FC (0.25),Terengganu FC (-0.25),Madura United FC (0.25)\"},{\"column_name\":\"1st_Half_Total_Goals_Asian\",\"value\":\"Under (0.75),Under (1.25),Under (1.75)\"},{\"column_name\":\"Rest_Of_The_Match__Current_Score_1_0\",\"value\":\"W1\"}]}'),
(2, '16020262', 1, '{\"uid\":1,\"game_id\":16020262,\"sport_id\":1,\"tableData\":[{\"column_name\":\"Match Result\",\"value\":\"W2\"},{\"column_name\":\"Double Chance\",\"value\":\"2 or Draw,12\"},{\"column_name\":\"Total Goals\",\"value\":\"Over (0.5),Over (1.5),Under (2),Over (2),Under (2.5),Under (3),Under (3.5),Under (4),Under (4.5),Under (5),Under (5.5)\"},{\"column_name\":\"Team 1 Total Goals\",\"value\":\"Under (0.5),Under (1),Under (1.5)\"},{\"column_name\":\"Team 2 Total Goals\",\"value\":\"Over (0.5),Over (1),Over (1.5),Under (2),Over (2),Under (2.5),Under (3),Under (3.5)\"},{\"column_name\":\"Goals Handicap\",\"value\":\"FC Nordsjaelland (3.5),FC Nordsjaelland (2.5),CSKA Moscow (-2),FC Nordsjaelland (2),CSKA Moscow (-1.5),CSKA Moscow (-1),CSKA Moscow (-0.5),CSKA Moscow (0),CSKA Moscow (0.5),CSKA Moscow (1),CSKA Moscow (1.5)\"},{\"column_name\":\"Goals Handicap 3 Way\",\"value\":\"Tie: CSKA Moscow (-2),CSKA Moscow (-1),CSKA Moscow (0),CSKA Moscow (1)\"},{\"column_name\":\"2nd Goal To Score\",\"value\":\"Team 2\"},{\"column_name\":\"3rd Goal To Score\",\"value\":\"No Goal\"},{\"column_name\":\"Draw No Bet\",\"value\":\"Team 2\"},{\"column_name\":\"Both Teams To Score\",\"value\":\"No\"},{\"column_name\":\"Total Goals Odd\\/Even\",\"value\":\"Even\"},{\"column_name\":\"Correct Score\",\"value\":\"0-2\"},{\"column_name\":\"1st Half Result\",\"value\":\"W2\"},{\"column_name\":\"1st Half Double Chance\",\"value\":\"X2,12\"},{\"column_name\":\"1st Half Total Goals\",\"value\":\"Over (0.5),Under (1),Over (1),Under (1.5),Under (2),Under (2.5)\"},{\"column_name\":\"1st Half Team 1 Total Goals\",\"value\":\"Under (0.5)\"},{\"column_name\":\"1st Half Team 2 Total Goals\",\"value\":\"Over (0.5),Under (1),Over (1),Under (1.5)\"},{\"column_name\":\"1st Half Goals Handicap\",\"value\":\"FC Nordsjaelland (1.5),CSKA Moscow (-1),FC Nordsjaelland (1),CSKA Moscow (-0.5),CSKA Moscow (0),CSKA Moscow (0.5)\"},{\"column_name\":\"1st Half Correct Score\",\"value\":\"0-1\"},{\"column_name\":\"Highest Scoring Half\",\"value\":\"1st = 2nd\"},{\"column_name\":\"2nd Half Result\",\"value\":\"W2\"},{\"column_name\":\"1st Half Goals Handicap 3 Way\",\"value\":\"Tie CSKA Moscow (-1),CSKA Moscow (0)\"},{\"column_name\":\"Goals Asian Handicap\",\"value\":\"FC Nordsjaelland (2.75),CSKA Moscow (-2.25),FC Nordsjaelland (2.25),CSKA Moscow (-1.75),FC Nordsjaelland (1.75),CSKA Moscow (-1.25),CSKA Moscow (-0.75),CSKA Moscow (-0.25),CSKA Moscow (0.25),CSKA Moscow (0.75),CSKA Moscow (1.25)\"},{\"column_name\":\"Total Goals Asian\",\"value\":\"Over (1.75),Under (1.75),Under (2.25),Over (2.25),Under (2.75),Under (3.25),Under (3.75),Under (4.25)\"},{\"column_name\":\"Total Goals 3 Way\",\"value\":\"Exactly (2),Under (3),Under (4)\"},{\"column_name\":\"1st Half Total Goals 3 Way\",\"value\":\"Exactly (1),Under (2)\"},{\"column_name\":\"1st Half Goals Asian Handicap\",\"value\":\"FC Nordsjaelland (1.75),FC Nordsjaelland (1.25),CSKA Moscow (-1.25),FC Nordsjaelland (0.75),CSKA Moscow (-0.75),CSKA Moscow (-0.25),CSKA Moscow (0.25)\"},{\"column_name\":\"1st Half Total Goals Asian\",\"value\":\"Over (0.75),Under (0.75),Under (1.25),Over (1.25),Under (1.75),Under (2.25)\"},{\"column_name\":\"Rest Of The Match (Current Score: 0-1)\",\"value\":\"W2\"},{\"column_name\":\"Rest Of The Match (Current Score: 0-2)\",\"value\":\"X\"}]}'),
(3, '16023727', 1, '{\"uid\":1,\"game_id\":16023727,\"sport_id\":1,\"tableData\":[{\"column_name\":\"Match Result\",\"value\":\"W1\"},{\"column_name\":\"Double Chance\",\"value\":\"12,1 or Draw\"},{\"column_name\":\"Total Goals\",\"value\":\"Over (1.5),Over (2),Over (2.5),Under (3),Over (3),Under (3.5),Under (4),Under (4.5),Under (5)\"},{\"column_name\":\"Team 1 Total Goals\",\"value\":\"Over (0.5),Over (1),Over (1.5),Under (2),Over (2),Under (2.5),Under (3),Under (3.5)\"},{\"column_name\":\"Team 2 Total Goals\",\"value\":\"Over (0.5),Under (1),Over (1),Under (1.5),Under (2),Under (2.5)\"},{\"column_name\":\"Goals Handicap\",\"value\":\"Polonia Bytom (1.5),Polonia Bytom (1),Polonia Bytom (0.5),Polonia Bytom (0),Polonia Bytom (-0.5),BKS Sarmacja Bedzin (1),Polonia Bytom (-1),BKS Sarmacja Bedzin (1.5),BKS Sarmacja Bedzin (2),BKS Sarmacja Bedzin (2.5),BKS Sarmacja Bedzin (3)\"},{\"column_name\":\"Goals Handicap 3 Way\",\"value\":\"Polonia Bytom (1),Polonia Bytom (0),Tie: BKS Sarmacja Bedzin (1),BKS Sarmacja Bedzin (2),BKS Sarmacja Bedzin (3)\"},{\"column_name\":\"2nd Goal To Score\",\"value\":\"Team 1\"},{\"column_name\":\"3rd Goal To Score\",\"value\":\"Team 2\"},{\"column_name\":\"Draw No Bet\",\"value\":\"Team 1\"},{\"column_name\":\"Both Teams To Score\",\"value\":\"Yes\"},{\"column_name\":\"Total Goals Odd\\/Even\",\"value\":\"Odd\"},{\"column_name\":\"Correct Score\",\"value\":\"2-1\"},{\"column_name\":\"1st Half Result\",\"value\":\"W1\"},{\"column_name\":\"1st Half Double Chance\",\"value\":\"12,1X\"},{\"column_name\":\"1st Half Total Goals\",\"value\":\"Over (0.5),Under (1),Over (1),Under (1.5),Under (2),Under (2.5)\"},{\"column_name\":\"1st Half Team 1 Total Goals\",\"value\":\"Over (0.5),Over (1),Under (1),Under (1.5)\"},{\"column_name\":\"1st Half Team 2 Total Goals\",\"value\":\"Under (0.5),Under (1)\"},{\"column_name\":\"1st Half Goals Handicap\",\"value\":\"Polonia Bytom (0.5),Polonia Bytom (0),Polonia Bytom (-0.5),BKS Sarmacja Bedzin (1),Polonia Bytom (-1),BKS Sarmacja Bedzin (1.5)\"},{\"column_name\":\"1st Half Correct Score\",\"value\":\"1-0\"},{\"column_name\":\"Highest Scoring Half\",\"value\":\"1st < 2nd\"},{\"column_name\":\"2nd Half Result\",\"value\":\"Draw\"},{\"column_name\":\"1st Half Goals Handicap 3 Way\",\"value\":\"Polonia Bytom (0),Tie BKS Sarmacja Bedzin (1)\"},{\"column_name\":\"Goals Asian Handicap\",\"value\":\"Polonia Bytom (1.25),Polonia Bytom (0.75),Polonia Bytom (0.25),Polonia Bytom (-0.25),BKS Sarmacja Bedzin (0.75),Polonia Bytom (-0.75),BKS Sarmacja Bedzin (1.25),Polonia Bytom (-1.25),BKS Sarmacja Bedzin (1.75),BKS Sarmacja Bedzin (2.25),BKS Sarmacja Bedzin (2.75),BKS Sarmacja Bedzin (3.25)\"},{\"column_name\":\"Total Goals Asian\",\"value\":\"Over (1.75),Over (2.25),Under (2.75),Over (2.75),Under (3.25),Over (3.25),Under (3.75),Under (4.25)\"},{\"column_name\":\"Total Goals 3 Way\",\"value\":\"Over (2),Exactly (3),Under (4)\"},{\"column_name\":\"1st Half Total Goals 3 Way\",\"value\":\"Exactly (1),Under (2)\"},{\"column_name\":\"1st Half Goals Asian Handicap\",\"value\":\"Polonia Bytom (0.25),Polonia Bytom (-0.25),Polonia Bytom (-0.75),BKS Sarmacja Bedzin (0.75),BKS Sarmacja Bedzin (1.25),Polonia Bytom (-1.25),BKS Sarmacja Bedzin (1.75)\"},{\"column_name\":\"1st Half Total Goals Asian\",\"value\":\"Under (0.75),Over (0.75),Under (1.25),Over (1.25),Under (1.75)\"},{\"column_name\":\"Rest Of The Match (Current Score: 1-0)\",\"value\":\"X\"}]}'),
(4, '15846489', 1, '{\"uid\":1,\"game_id\":15846489,\"sport_id\":1,\"tableData\":[{\"column_name\":\"Match Result\",\"value\":\"W1\"},{\"column_name\":\"Double Chance\",\"value\":\"12,1 or Draw\"},{\"column_name\":\"Total Goals\",\"value\":\"Over (0.5),Over (1),Over (1.5),Over (2),Over (2.5),Under (3),Over (3),Under (3.5),Under (4),Under (4.5),Under (5),Under (5.5),Under (6),Under (6.5),Under (7.5)\"},{\"column_name\":\"Team 1 Total Goals\",\"value\":\"Over (0.5),Over (1),Over (1.5),Under (2),Over (2),Under (2.5),Under (3),Under (3.5),Under (4),Under (4.5)\"},{\"column_name\":\"Team 2 Total Goals\",\"value\":\"Over (0.5),Under (1),Over (1),Under (1.5),Under (2),Under (2.5)\"},{\"column_name\":\"Goals Handicap\",\"value\":\"Bayer Leverkusen (3),Bayer Leverkusen (1.5),Bayer Leverkusen (1),Bayer Leverkusen (0.5),Bayer Leverkusen (0),Bayer Leverkusen (-0.5),VfB Stuttgart (1),Bayer Leverkusen (-1),VfB Stuttgart (1.5),VfB Stuttgart (2),VfB Stuttgart (2.5),VfB Stuttgart (3),VfB Stuttgart (3.5),VfB Stuttgart (4),VfB Stuttgart (4.5)\"},{\"column_name\":\"Goals Handicap 3 Way\",\"value\":\"Bayer Leverkusen (1),Tie: VfB Stuttgart (1),VfB Stuttgart (2),VfB Stuttgart (3),VfB Stuttgart (4)\"},{\"column_name\":\"1st Goal Time\",\"value\":\"71-80\"},{\"column_name\":\"2nd Goal Time\",\"value\":\"81-90+\"},{\"column_name\":\"1st Goal To Score\",\"value\":\"Team 1\"},{\"column_name\":\"2nd Goal To Score\",\"value\":\"Team 1\"},{\"column_name\":\"3rd Goal To Score\",\"value\":\"Team 2\"},{\"column_name\":\"4th Goal To Score\",\"value\":\"No Goal\"},{\"column_name\":\"Draw No Bet\",\"value\":\"Team 1\"},{\"column_name\":\"Both Teams To Score\",\"value\":\"Yes\"},{\"column_name\":\"Total Goals Odd\\/Even\",\"value\":\"Odd\"},{\"column_name\":\"Total Goals (Bands)\",\"value\":\"2 or 3\"},{\"column_name\":\"Team 1 Total Goals (Bands)\",\"value\":\"2 or 3\"},{\"column_name\":\"Team 2 Total Goals (Bands)\",\"value\":\"0 or 1\"},{\"column_name\":\"Correct Score\",\"value\":\"2-1\"},{\"column_name\":\"Outcome And Total Goals 2.5\",\"value\":\"12 and Over 2.5,1X and Over 2.5,W1 and Over 2.5\"},{\"column_name\":\"Outcome And Total Goals 3.5\",\"value\":\"1X and Under 3.5,12 and Under 3.5,W1 and Under 3.5\"},{\"column_name\":\"1st Half Result\",\"value\":\"Draw\"},{\"column_name\":\"1st Half Double Chance\",\"value\":\"X2,1X\"},{\"column_name\":\"1st Half Total Goals\",\"value\":\"Under (0.5),Under (1),Under (1.5),Under (2),Under (2.5),Under (3),Under (3.5),Under (4.5)\"},{\"column_name\":\"1st Half Team 1 Total Goals\",\"value\":\"Under (0.5),Under (1),Under (1.5),Under (2),Under (2.5)\"},{\"column_name\":\"1st Half Team 2 Total Goals\",\"value\":\"Under (0.5),Under (1),Under (1.5)\"},{\"column_name\":\"1st Half Goals Handicap\",\"value\":\"Bayer Leverkusen (0.5),VfB Stuttgart (0),Bayer Leverkusen (0),VfB Stuttgart (0.5),VfB Stuttgart (1),VfB Stuttgart (1.5),VfB Stuttgart (2),VfB Stuttgart (2.5)\"},{\"column_name\":\"1st Half Total Goals (Bands)\",\"value\":\"0 or 1\"},{\"column_name\":\"1st Half Team 1 Total Goals (Bands)\",\"value\":\"0 or 1,0 or 1\"},{\"column_name\":\"1st Half Team 2 Total Goals (Bands)\",\"value\":\"0 or 1,0 or 1\"},{\"column_name\":\"1st Half Correct Score\",\"value\":\"0-0\"},{\"column_name\":\"Corners: Result\",\"value\":\"W1\"},{\"column_name\":\"Corners: Total\",\"value\":\"Over (5.5),Over (6),Over (6.5),Over (7),Over (7.5),Under (8),Over (8),Under (8.5),Under (9),Under (9.5),Under (10),Under (10.5),Under (11),Under (11.5),Under (12),Under (12.5),Under (13),Under (13.5),Under (14),Under (14.5),Under (15),Under (15.5)\"},{\"column_name\":\"Corners: Team 1 Total\",\"value\":\"Over (2.5),Over (3.5),Over (4.5),Over (5.5),Over (6.5),Under (7.5),Under (8.5),Under (9.5)\"},{\"column_name\":\"Corners: Team 2 Total\",\"value\":\"Under (1.5),Under (2.5),Under (3.5),Under (4.5),Under (5.5),Under (6.5),Under (7.5)\"},{\"column_name\":\"Total Red Cards\",\"value\":\"Under (0.5)\"},{\"column_name\":\"Yellow Cards: Result\",\"value\":\"W1\"},{\"column_name\":\"Yellow Cards: Total\",\"value\":\"Over (1.5),Over (2.5),Under (3.5),Under (4.5),Under (5.5),Under (6.5)\"},{\"column_name\":\"Yellow Cards: Team 1 Total\",\"value\":\"Over (1.5),Under (2.5)\"},{\"column_name\":\"Yellow Cards: Team 2 Total\",\"value\":\"Under (1.5),Under (2.5),Under (3.5)\"},{\"column_name\":\"Half Time\\/Full-time\",\"value\":\"Draw\\/Bayer Leverkusen\"},{\"column_name\":\"Highest Scoring Half\",\"value\":\"1st < 2nd\"},{\"column_name\":\"2nd Half Result\",\"value\":\"W1\"},{\"column_name\":\"Team 1 Total Goals (Exact)\",\"value\":\"2\"},{\"column_name\":\"Team 2 Total Goals (Exact)\",\"value\":\"1\"},{\"column_name\":\"1st Half Total Goals Odd\\/Even\",\"value\":\"Even\"},{\"column_name\":\"First Team to Score\",\"value\":\"Bayer Leverkusen\"},{\"column_name\":\"Goals In Both Halves\",\"value\":\"No\"},{\"column_name\":\"Last Team to Score\",\"value\":\"VfB Stuttgart\"},{\"column_name\":\"Team 1 Score in Both Halves\",\"value\":\"No\"},{\"column_name\":\"Team 1 To Score\",\"value\":\"Yes\"},{\"column_name\":\"Team 2 Score in Both Halves\",\"value\":\"No\"},{\"column_name\":\"Team 2 To Score\",\"value\":\"Yes\"},{\"column_name\":\"Team 2 Win Both Halves\",\"value\":\"No\"},{\"column_name\":\"Team 1 Win Both Halves\",\"value\":\"No\"},{\"column_name\":\"Team 1 Score in First Half\",\"value\":\"No\"},{\"column_name\":\"Team 1 Score in Second Half\",\"value\":\"Yes\"},{\"column_name\":\"Team 2 Score in First Half\",\"value\":\"No\"},{\"column_name\":\"2nd Half Correct Score\",\"value\":\"2-1\"},{\"column_name\":\"1st Half Both Teams To Score\",\"value\":\"No\"},{\"column_name\":\"2nd Half Both Teams To Score\",\"value\":\"Yes\"},{\"column_name\":\"Corners: 1st Half Total\",\"value\":\"Over (4.5),Under (5),Over (5),Over (1.5),Over (2.5),Over (3.5),Over (4.5),Under (5.5),Under (6.5),Under (7.5)\"},{\"column_name\":\"Corners: 2nd Half Total\",\"value\":\"Under (5),Under (5.5)\"},{\"column_name\":\"Yellow Cards: 1st Half Total\",\"value\":\"Under (1.5)\"},{\"column_name\":\"2nd Half Goals Handicap\",\"value\":\"Bayer Leverkusen (1.5),Bayer Leverkusen (1),Bayer Leverkusen (0),VfB Stuttgart (1),Bayer Leverkusen (-1),VfB Stuttgart (1.5),VfB Stuttgart (2),VfB Stuttgart (2.5)\"},{\"column_name\":\"2nd Half Total Goals\",\"value\":\"Over (0.5),Over (1),Over (1.5),Over (2),Over (2.5),Under (3),Over (3),Under (3.5),Under (4),Under (4.5)\"},{\"column_name\":\"Goal in First Half\",\"value\":\"No\"},{\"column_name\":\"Goal in Second Half\",\"value\":\"Yes\"},{\"column_name\":\"Team 2 Score in Second Half\",\"value\":\"Yes\"},{\"column_name\":\"Last Goalscorer\",\"value\":\"Tanguy Coulibaly,Borna Sosa,Philipp Klement,Filho Filho Paulinho,Silas Wamangituka Fundu,Marc-Oliver Kempf,Darko Churlinov,Julian Baumgartlinger,Holger Badstuber,Panagiotis Retsos,Kevin Volland,Antonis Aidonis,Lars Bender,Atakan Karazor,Mario Gomez,Hamadi Al Ghaddioui,Leon Bailey,Adrian Stanilewicz,Maxime Awoudja,Ayman Azhil\"},{\"column_name\":\"First Goalscorer\",\"value\":\"Mario Gomez,Ayman Azhil,Panagiotis Retsos,Holger Badstuber,Borna Sosa,Lucas Alario,Julian Baumgartlinger,Philipp Klement,Marc-Oliver Kempf,Lars Bender,Tanguy Coulibaly,Maxime Awoudja,Antonis Aidonis,Borges Wendell,Darko Churlinov,Filho Filho Paulinho,Atakan Karazor,Adrian Stanilewicz,Kevin Volland,Leon Bailey,Hamadi Al Ghaddioui\"},{\"column_name\":\"Anytime Goalscorer\",\"value\":\"Tanguy Coulibaly,Holger Badstuber,Atakan Karazor,Antonis Aidonis,Maxime Awoudja,Borna Sosa,Panagiotis Retsos,Marc-Oliver Kempf,Filho Filho Paulinho,Mario Gomez,Lucas Alario,Leon Bailey,Kevin Volland,Hamadi Al Ghaddioui,Lars Bender,Ayman Azhil,Julian Baumgartlinger,Silas Wamangituka Fundu,Philipp Klement,Darko Churlinov,Adrian Stanilewicz\"},{\"column_name\":\"Player To Score 3 Or More\",\"value\":\"Hamadi Al Ghaddioui,Holger Badstuber,Darko Churlinov,Mario Gomez,Antonis Aidonis,Philipp Klement,Tanguy Coulibaly,Maxime Awoudja,Borna Sosa,Julian Baumgartlinger,Kevin Volland,Panagiotis Retsos,Ayman Azhil,Atakan Karazor,Lars Bender,Marc-Oliver Kempf,Adrian Stanilewicz,Filho Filho Paulinho,Leon Bailey\"},{\"column_name\":\"Player To Score 2 Or More\",\"value\":\"Darko Churlinov,Filho Filho Paulinho,Hamadi Al Ghaddioui,Marc-Oliver Kempf,Leon Bailey,Holger Badstuber,Atakan Karazor,Antonis Aidonis,Kevin Volland,Lars Bender,Borna Sosa,Adrian Stanilewicz,Tanguy Coulibaly,Julian Baumgartlinger,Maxime Awoudja,Panagiotis Retsos,Philipp Klement,Ayman Azhil,Mario Gomez\"},{\"column_name\":\"Corners: Handicap\",\"value\":\"Bayer Leverkusen (0),Bayer Leverkusen (-0.5),Bayer Leverkusen (-1),Bayer Leverkusen (-1.5),Bayer Leverkusen (-2),Bayer Leverkusen (-2.5),Bayer Leverkusen (-3),Bayer Leverkusen (-3.5),Bayer Leverkusen (-4),Bayer Leverkusen (-4.5),Bayer Leverkusen (-5),Bayer Leverkusen (-5.5),Bayer Leverkusen (-6),VfB Stuttgart (6),VfB Stuttgart (6.5),VfB Stuttgart (7),VfB Stuttgart (7.5)\"},{\"column_name\":\"Corners: Odd\\/Even\",\"value\":\"Even\"},{\"column_name\":\"Own goal\",\"value\":\"Yes\"},{\"column_name\":\"1st Half Goals Handicap 3 Way\",\"value\":\"Tie VfB Stuttgart (0),VfB Stuttgart (1),VfB Stuttgart (2)\"},{\"column_name\":\"2nd Half Goals Handicap 3 Way\",\"value\":\"Bayer Leverkusen (1),Bayer Leverkusen (0),Tie VfB Stuttgart (1),VfB Stuttgart (2)\"},{\"column_name\":\"Outcome And Total Goals 4.5\",\"value\":\"1X and Under 4.5,12 and Under 4.5,W1 and Under 4.5\"},{\"column_name\":\"Outcome And Both To Score\",\"value\":\"W1 And (Both To Score - Yes)\"},{\"column_name\":\"Team 1 To Win To Nil\",\"value\":\"No\"},{\"column_name\":\"Team 2 To Win To Nil\",\"value\":\"No\"},{\"column_name\":\"1-15 Min. Winner\",\"value\":\"X\"},{\"column_name\":\"1-30 Min. Winner\",\"value\":\"X\"},{\"column_name\":\"1-60 Min. Winner\",\"value\":\"X\"},{\"column_name\":\"1-75 Min. Winner\",\"value\":\"W1\"},{\"column_name\":\"Team 1 Win By Two or Three Goals\",\"value\":\"No\"},{\"column_name\":\"Team 2 Win By Two or Three Goals\",\"value\":\"No\"},{\"column_name\":\"Team 1 Win By One Goal or Draw\",\"value\":\"Yes\"},{\"column_name\":\"Team 2 Win By One Goal or Draw\",\"value\":\"No\"},{\"column_name\":\"1st Half Or Match Result\",\"value\":\"X,W1\"},{\"column_name\":\"Team 2 Will Win at Least in One of The Halves\",\"value\":\"No\"},{\"column_name\":\"Team 1 Will Win at Least in One of The Halves\",\"value\":\"Yes\"},{\"column_name\":\"Team 1 Will Win 1st Half and Won\'t Win The Match\",\"value\":\"No\"},{\"column_name\":\"Team 2 Will Win 1st Half and Won\'t Win The Match\",\"value\":\"No\"},{\"column_name\":\"Match Score Draw\",\"value\":\"No\"},{\"column_name\":\"Half With Most Goals\",\"value\":\"1 < 2\"},{\"column_name\":\"Exactly 1 Goal in The Match\",\"value\":\"No\"},{\"column_name\":\"Exactly 2 Goal in The Match\",\"value\":\"No\"},{\"column_name\":\"Exactly 3 Goal in The Match\",\"value\":\"Yes\"},{\"column_name\":\"Exactly 4 Goal in The Match\",\"value\":\"No\"},{\"column_name\":\"Score Combinations\",\"value\":\"2-1 \\/ 3-1 \\/ 4-1\"},{\"column_name\":\"1-15 Min. Goals Handicap\",\"value\":\"VfB Stuttgart (0),Bayer Leverkusen (0)\"},{\"column_name\":\"1-15 Min. Total Goals\",\"value\":\"Under (0.5),Under (1),Under (1.5)\"},{\"column_name\":\"1-30 Min. Goals Handicap\",\"value\":\"VfB Stuttgart (0),Bayer Leverkusen (0)\"},{\"column_name\":\"1-30 Min. Total Goals\",\"value\":\"Under (0.5),Under (1),Under (1.5)\"},{\"column_name\":\"1-60 MIn. Goals Handicap\",\"value\":\"Bayer Leverkusen (0),VfB Stuttgart (0),VfB Stuttgart (1)\"},{\"column_name\":\"1-60 Min. Total Goals\",\"value\":\"Under (0.5),Under (1.5),Under (2),Under (2.5)\"},{\"column_name\":\"1-75 Min. Goals Handicap\",\"value\":\"Bayer Leverkusen (0),VfB Stuttgart (1),Bayer Leverkusen (-1)\"},{\"column_name\":\"1-75 Min. Total Goals\",\"value\":\"Over (0.5),Under (1.5),Under (2),Under (2.5),Under (3),Under (3.5)\"},{\"column_name\":\"Team 1 Winning Margin\",\"value\":\"1\"},{\"column_name\":\"Team 2 Winning Margin\",\"value\":\"\"},{\"column_name\":\"Team 1 To Score First Half \\/Second Half\",\"value\":\"No\\/Yes\"},{\"column_name\":\"Team 2 To Score First Half \\/Second Half\",\"value\":\"No\\/Yes\"},{\"column_name\":\"Exact Number Of Goals\",\"value\":\"Exactly 3\"},{\"column_name\":\"Yellow Cards: Double Chance\",\"value\":\"1X,12\"},{\"column_name\":\"Yellow Cards: Handicap\",\"value\":\"Bayer Leverkusen (3.5),Bayer Leverkusen (2.5),Bayer Leverkusen (1.5),Bayer Leverkusen (1),Bayer Leverkusen (0.5),Bayer Leverkusen (0),Bayer Leverkusen (-0.5),VfB Stuttgart (1.5) \"},{\"column_name\":\"Corners: 1st Half Team 1 Total\",\"value\":\"Over (2.5),Over (3),Over (3.5)\"},{\"column_name\":\"Corners: 1st Half Team 2 Total\",\"value\":\"Under (1),Over (1),Under (1.5),Under (2)\"},{\"column_name\":\"To Qualify\",\"value\":\"W1,W1\"},{\"column_name\":\"Goals Asian Handicap\",\"value\":\"Bayer Leverkusen (1.25),Bayer Leverkusen (0.75),Bayer Leverkusen (0.25),Bayer Leverkusen (-0.25),VfB Stuttgart (0.75),Bayer Leverkusen (-0.75),VfB Stuttgart (1.25),Bayer Leverkusen (-1.25),VfB Stuttgart (1.75),VfB Stuttgart (2.25),VfB Stuttgart (2.75),VfB Stuttgart (3.25),VfB Stuttgart (3.75),VfB Stuttgart (4.25)\"},{\"column_name\":\"Total Goals Asian\",\"value\":\"Over (0.75),Over (1.25),Over (1.75),Over (2.25),Under (2.75),Over (2.75),Under (3.25),Over (3.25),Under (3.75),Under (4.25),Under (4.75),Under (5.25),Under (5.75),Under (6.25),Under (6.75)\"},{\"column_name\":\"Total Goals (Exact)\",\"value\":\"3\"},{\"column_name\":\"Chance Mix\",\"value\":\"Draw or under 3.5 goals,Draw or over 1.5 goals,Both teams to score yes or under 2.5 goals,Both teams to score yes or over 2.5 goals,Both teams to score no or over 2.5 goals,Both teams to score yes or max 1 goal total,Team 1 win or over 1.5 goals,Team 2 win or over 1.5 goals,Team 1 win or correct score 0-0,Team 1 win or correct score 0-0 , 0-1,Team 2 win or under 3,5 goals,Team 1 win or both teams to score yes,Team 1 win or under 2,5 goals,Team 1 win or over 2.5 goals,Draw or over 2,5 goals,Team 2 win or over 2,5 goals,Draw or both teams to score yes,Team 1 win or over 3.5 goals,Team 1 win or under 3.5 goals,Team 2 win or both teams to score yes,Team 1 win or both teams to score no\"},{\"column_name\":\"Double Chance Combo\",\"value\":\"Team1 or team2 will win ,and there will be 3 goals or less,Team1 will win or draw,and there will be 2 goals or more,Team1 will win or draw,and there will be 2-3 goals,Team1 or team2 will win ,and there will be 2-3 goals,Team1 or team2 will win ,and there will be 2 goals or more,Team1 will win or draw,and there will be 3 goals or less,Team1 or team2 will win ,and there will be 3 goals or more,Team1 will win or draw,and there will be 3 goals or more,Team1 will win or draw,and both teams will score,Team1 or team2 will win ,and  both teams will score\"},{\"column_name\":\"Corners: 1st Half Handicap\",\"value\":\"Bayer Leverkusen (-1.5),Bayer Leverkusen (-2)\"},{\"column_name\":\"Corners: 1st Half Result\",\"value\":\"W1\"},{\"column_name\":\"2nd Half Total Goals Odd\\/Even\",\"value\":\"Odd\"},{\"column_name\":\"1st Half\\/2nd Half Both To Score\",\"value\":\"No\\/Yes\"},{\"column_name\":\"1st Half 1st Goal To Score\",\"value\":\"No Goal\"},{\"column_name\":\"Outcome And Total Goals 1.5\",\"value\":\"12 and Over 1.5,1X and Over 1.5,W1 and Over 1.5\"},{\"column_name\":\"Team 1 No Bet\",\"value\":\"Team 2,Draw\"},{\"column_name\":\"Team 2 No Bet\",\"value\":\"Team 1\"},{\"column_name\":\"Double Chance And Both To Score\",\"value\":\"1X And (Both To Score - Yes),12 And (Both To Score - Yes)\"},{\"column_name\":\"Corners: 2nd Half Result\",\"value\":\"W1\"},{\"column_name\":\"Corners: 2nd Half Handicap\",\"value\":\"Bayer Leverkusen (-1.5),Bayer Leverkusen (-2)\"},{\"column_name\":\"2nd Half Double Chance\",\"value\":\"12,1X\"},{\"column_name\":\"Both Teams To Score And Total Goals 2.5\",\"value\":\"Yes and Over 2.5\"},{\"column_name\":\"Both Teams To Score And Total Goals 3.5\",\"value\":\"Yes and Under 3.5\"},{\"column_name\":\"Both Half Less Than 1.5 Goal\",\"value\":\"No\"},{\"column_name\":\"Corners: First Corner\",\"value\":\"Team 1\"},{\"column_name\":\"Corners: Last Corner\",\"value\":\"Team 1\"},{\"column_name\":\"Half With The Most Corners\",\"value\":\"1 > 2\"},{\"column_name\":\"1st Half Total Goals Asian\",\"value\":\"Under (0.75),Under (1.25),Under (1.75),Under (2.25),Under (2.75),Under (3.25),Under (3.75),Under (0.75),Under (1.25),Under (1.75)\"},{\"column_name\":\"1st Half Goals Asian Handicap\",\"value\":\"Bayer Leverkusen (0.75),VfB Stuttgart (-0.25),Bayer Leverkusen (0.25),VfB Stuttgart (0.25),Bayer Leverkusen (-0.25),VfB Stuttgart (0.75),VfB Stuttgart (1.25),VfB Stuttgart (1.75),VfB Stuttgart (-0.25),Bayer Leverkusen (0.25),VfB Stuttgart (0.25),Bayer Leverkusen (-0.25),VfB Stuttgart (0.75)\"},{\"column_name\":\"Team 1 Total Goals Odd\\/Even\",\"value\":\"Even\"},{\"column_name\":\"Team 2 Total Goals Odd\\/Even\",\"value\":\"Odd\"},{\"column_name\":\"First Team To Score And Match Result\",\"value\":\"Bayer Leverkusen and Bayer Leverkusen\"},{\"column_name\":\"Each Team To Score Over (1.5)\",\"value\":\"No\"},{\"column_name\":\"Each Team To Score Over (2.5)\",\"value\":\"No\"},{\"column_name\":\"Each Team To Score Under (1.5)\",\"value\":\"No\"},{\"column_name\":\"Each Team To Score Under (2.5)\",\"value\":\"Yes\"},{\"column_name\":\"Both Halves Total Over (1.5)\",\"value\":\"No\"},{\"column_name\":\"Both Halves Total Over (2.5)\",\"value\":\"No\"},{\"column_name\":\"Bayer Leverkusen Both Halves Total Over (0.5)\",\"value\":\"No\"},{\"column_name\":\"Bayer Leverkusen Both Halves Total Over (1.5)\",\"value\":\"No\"},{\"column_name\":\"VfB Stuttgart Both Halves Total Over (0.5)\",\"value\":\"No\"},{\"column_name\":\"VfB Stuttgart Both Halves Total Over (1.5)\",\"value\":\"No\"},{\"column_name\":\"1st Half Total Goals (Exact)\",\"value\":\"0\"},{\"column_name\":\"First Half\\/Second Half Result\",\"value\":\"Draw\\/W1\"},{\"column_name\":\"First Half Total Goals Vs Second Half Total Goals Handicap\",\"value\":\"Second Half Total Goals (-2),Second Half Total Goals (-1.5),Second Half Total Goals (-1),Second Half Total Goals (-0.5),Second Half Total Goals (0)\"},{\"column_name\":\"Team 1 Total Goals Asian\",\"value\":\"Over (0.75),Over (1.25),Under (1.75),Over (1.75),Under (2.25),Over (2.25),Under (2.75),Under (3.25),Under (3.75)\"},{\"column_name\":\"Team 2 Total Goals Asian\",\"value\":\"Under (0.75),Over (0.75),Over (1.25),Under (1.25),Under (1.75),Under (2.25)\"},{\"column_name\":\"Total Goals 3 Way\",\"value\":\"Over (0),Over (1),Over (2),Exactly (3),Under (4)\"},{\"column_name\":\"1st Half Total Goals 3 Way\",\"value\":\"Under (1),Under (2)\"},{\"column_name\":\"Corners: Correct Score\",\"value\":\"7-1\"},{\"column_name\":\"Yellow Cards: Correct Score\",\"value\":\"2-1\"},{\"column_name\":\"The 1st Goal Will be Scored\",\"value\":\"Own Goal\"},{\"column_name\":\"The 2nd Goal Will be Scored\",\"value\":\"By Other Method\"},{\"column_name\":\"The 3rd Goal Will be Scored\",\"value\":\"By Other Method\"},{\"column_name\":\"2nd Half Team 1 Total Goals\",\"value\":\"Over (0.5),Over (1),Over (1.5),Under (2),Over (2),Under (2.5),Under (3),Under (3.5)\"},{\"column_name\":\"2nd Half Team 2 Total Goals\",\"value\":\"Over (0.5),Under (1),Over (1),Under (1.5)\"},{\"column_name\":\"Team 1 Win By Exact (1) Goal\",\"value\":\"Yes\"},{\"column_name\":\"Team 1 Win By Exact (2) Goal\",\"value\":\"No\"},{\"column_name\":\"Team 1 Win By Exact (3) Goal\",\"value\":\"No\"},{\"column_name\":\"Team 1 Win By Exact (4) Goal\",\"value\":\"No\"},{\"column_name\":\"Team 2 Win By Exact (1) Goal\",\"value\":\"No\"},{\"column_name\":\"1st Half Team 1 Win By Exact (1) Goal\",\"value\":\"No\"},{\"column_name\":\"1st Half Team 1 Win By Exact (2) Goal\",\"value\":\"No\"},{\"column_name\":\"1st Half Team 1 Win By Exact (3) Goal\",\"value\":\"No\"},{\"column_name\":\"1st Half Team 2 Win By Exact (1) Goal\",\"value\":\"No\"},{\"column_name\":\"1st Half Team 2 Win By Exact (2) Goal\",\"value\":\"No\"},{\"column_name\":\"2nd Half Team 1 Win By Exact (1) Goal\",\"value\":\"Yes\"},{\"column_name\":\"2nd Half Team 1 Win By Exact (2) Goal\",\"value\":\"No\"},{\"column_name\":\"2nd Half Team 1 Win By Exact (3) Goal\",\"value\":\"No\"},{\"column_name\":\"2nd Half Team 2 Win By Exact (1) Goal\",\"value\":\"No\"},{\"column_name\":\"2nd Half Team 2 Win By Exact (2) Goal\",\"value\":\"No\"},{\"column_name\":\"1st Half Team 1 Total Goals (Exact)\",\"value\":\"0\"},{\"column_name\":\"1st Half Team 2 Total Goals (Exact)\",\"value\":\"0\"},{\"column_name\":\"2nd Half Total Goals (Exact)\",\"value\":\"3\"},{\"column_name\":\"2nd Half Team 1 Total Goals (Exact)\",\"value\":\"2+\"},{\"column_name\":\"2nd Half Team 2 Total Goals (Exact)\",\"value\":\"1\"},{\"column_name\":\"2nd Half Total Goals (Bands)\",\"value\":\"2 or 3\"},{\"column_name\":\"2nd Half Team 1 Total Goals (Bands)\",\"value\":\"2 or 3\"},{\"column_name\":\"2nd Half Team 2 Total Goals (Bands)\",\"value\":\"0 or 1\"},{\"column_name\":\"1st Half Team 1 To Win To Nil\",\"value\":\"No\"},{\"column_name\":\"1st Half Team 2 To Win To Nil\",\"value\":\"No\"},{\"column_name\":\"2nd Half Team 1 To Win To Nil\",\"value\":\"No\"},{\"column_name\":\"2nd Half Team 2 To Win To Nil\",\"value\":\"No\"},{\"column_name\":\"Both Halves Will Win Different Teams\",\"value\":\"No\"},{\"column_name\":\"Team 1 Will Win and Score Exact (1) Goal\",\"value\":\"No\"},{\"column_name\":\"Team 1 Will Win and Score Exact (2) Goal\",\"value\":\"Yes\"},{\"column_name\":\"Team 1 Will Win and Score Exact (3) Goal\",\"value\":\"No\"},{\"column_name\":\"Team 1 Will Win and Score Exact (4) Goal\",\"value\":\"No\"},{\"column_name\":\"Team 2 Will Win and Score Exact (2) Goal\",\"value\":\"No\"},{\"column_name\":\"1st Half First Team to Score\",\"value\":\"No Goal\"},{\"column_name\":\"1st Half Last Team to Score\",\"value\":\"No Goal\"},{\"column_name\":\"2nd Half First Team to Score\",\"value\":\"Bayer Leverkusen\"},{\"column_name\":\"2nd Half Last Team to Score\",\"value\":\"VfB Stuttgart\"},{\"column_name\":\"Corners: Race To (3)\",\"value\":\"Bayer Leverkusen\"},{\"column_name\":\"Corners: Race To (5)\",\"value\":\"Bayer Leverkusen\"},{\"column_name\":\"Corners: Race To (7)\",\"value\":\"Bayer Leverkusen\"},{\"column_name\":\"Corners: Race To (9)\",\"value\":\"Neither\"},{\"column_name\":\"1-15 Min. Double Chance\",\"value\":\"1X,X2\"},{\"column_name\":\"1-30 Min. Double Chance\",\"value\":\"X2,1X\"},{\"column_name\":\"1-60 Min. Double Chance\",\"value\":\"1X,X2\"},{\"column_name\":\"1-75 Min. Double Chance\",\"value\":\"12,1X\"},{\"column_name\":\"1-15 Min. Team 1 Total Goals\",\"value\":\"Under (0.5),Under (1)\"},{\"column_name\":\"1-15 Min. Team 2 Total Goals\",\"value\":\"Under (0.5),Under (1)\"},{\"column_name\":\"1-30 Min. Team 1 Total Goals\",\"value\":\"Under (0.5),Under (1)\"},{\"column_name\":\"1-30 Min. Team 2 Total Goals\",\"value\":\"Under (0.5),Under (1)\"},{\"column_name\":\"1-60 Min. Team 1 Total Goals\",\"value\":\"Under (0.5),Under (1),Under (1.5)\"},{\"column_name\":\"1-60 Min. Team 2 Total Goals\",\"value\":\"Under (0.5),Under (1)\"},{\"column_name\":\"1-75 Min. Team 1 Total Goals\",\"value\":\"Over (0.5),Under (1.5),Under (2),Under (2.5)\"},{\"column_name\":\"1-75 Min. Team 2 Total Goals\",\"value\":\"Under (0.5),Under (1)\"},{\"column_name\":\"Team 1 To Score and Match Result\",\"value\":\"Yes and W1\"},{\"column_name\":\"Team 2 To Score and Match Result\",\"value\":\"Yes and W1\"},{\"column_name\":\"Outcome And Total Goals (Exact)\",\"value\":\"W1 and 3\"},{\"column_name\":\"Outcome or Correct Score\",\"value\":\"W1 or 0-1: Yes,W2 or 1-1: No,W2 or 1-0: No,W1 or 0-0: Yes,W1 or 1-1: Yes,W2 or 0-0: No\"},{\"column_name\":\"Outcome or Total Goals (2.5)\",\"value\":\"W2 or Over,W1 or Over,W1 or Under,Draw or Over\"},{\"column_name\":\"Outcome or Total Goals (3.5)\",\"value\":\"Draw or Under,W2 or Under,W1 or Over,W1 or Under\"},{\"column_name\":\"Outcome or Total Goals (4.5)\",\"value\":\"Draw or Under,W2 or Under,W1 or Over,W1 or Under\"},{\"column_name\":\"Corners: 2nd Half Team 1 Total\",\"value\":\"Under (3),Over (3),Under (3.5),Under (4)\"},{\"column_name\":\"Corners: 2nd Half Team 2 Total\",\"value\":\"Under (1),Under (1.5),Under (2)\"},{\"column_name\":\"Team 1 First Goalscorer\",\"value\":\"Borges Wendell,Panagiotis Retsos,Lars Bender,Adrian Stanilewicz,Kevin Volland,Ayman Azhil,Julian Baumgartlinger,Lucas Alario,Filho Filho Paulinho,Leon Bailey\"},{\"column_name\":\"Team 2 First Goalscorer\",\"value\":\"Mario Gomez,Philipp Klement,Atakan Karazor,Hamadi Al Ghaddioui,Maxime Awoudja,Tanguy Coulibaly,Antonis Aidonis,Silas Wamangituka Fundu,Marc-Oliver Kempf,Borna Sosa,Holger Badstuber,Darko Churlinov\"},{\"column_name\":\"Match Victory Method\",\"value\":\"Bayer Leverkusen Win 90+ Min\"},{\"column_name\":\"Rest Of The Match (Current Score: 1-0)\",\"value\":\"X\"},{\"column_name\":\"Team 1 Last Goalscorer\",\"value\":\"Kevin Volland,Ayman Azhil,Julian Baumgartlinger,Adrian Stanilewicz,Lucas Alario,Panagiotis Retsos,Leon Bailey,Filho Filho Paulinho,Lars Bender\"},{\"column_name\":\"Team 2 Last Goalscorer\",\"value\":\"Atakan Karazor,Hamadi Al Ghaddioui,Maxime Awoudja,Holger Badstuber,Tanguy Coulibaly,Marc-Oliver Kempf,Philipp Klement,Darko Churlinov,Mario Gomez,Borna Sosa,Silas Wamangituka Fundu,Antonis Aidonis\"}]}'),
(5, '16021912', 1, '{\"uid\":1,\"game_id\":16021912,\"sport_id\":1,\"tableData\":[{\"column_name\":\"Match Result\",\"value\":\"W2\"},{\"column_name\":\"Double Chance\",\"value\":\"12,2 or Draw\"},{\"column_name\":\"Total Goals\",\"value\":\"Over (0.5),Over (1),Over (1.5),Over (2),Over (2.5),Under (3),Over (3),Under (3.5),Under (4),Under (4.5)\"},{\"column_name\":\"Team 1 Total Goals\",\"value\":\"Over (0.5),Under (1),Over (1),Under (1.5),Under (2),Under (2.5)\"},{\"column_name\":\"Team 2 Total Goals\",\"value\":\"Over (0.5),Over (1),Over (1.5),Over (2),Under (2)\"},{\"column_name\":\"Goals Handicap\",\"value\":\"SCG Muangthong United (1.5),SCG Muangthong United (1),Port FC (-1),Port FC (-0.5),Port FC (0),Port FC (0.5),Port FC (1),Port FC (1.5),Port FC (2)\"},{\"column_name\":\"Goals Handicap 3 Way\",\"value\":\"Tie: Port FC (-1),Port FC (0),Port FC (1)\"},{\"column_name\":\"1st Goal To Score\",\"value\":\"Team 1\"},{\"column_name\":\"2nd Goal To Score\",\"value\":\"Team 2\"},{\"column_name\":\"3rd Goal To Score\",\"value\":\"Team 2\"},{\"column_name\":\"Draw No Bet\",\"value\":\"Team 2\"},{\"column_name\":\"Both Teams To Score\",\"value\":\"Yes\"},{\"column_name\":\"Total Goals Odd\\/Even\",\"value\":\"Odd\"},{\"column_name\":\"Correct Score\",\"value\":\"1-2\"},{\"column_name\":\"1st Half Result\",\"value\":\"W1\"},{\"column_name\":\"1st Half Double Chance\",\"value\":\"12,1X\"},{\"column_name\":\"1st Half Total Goals\",\"value\":\"Over (0.5),Under (1),Over (1),Under (1.5),Under (2),Under (2.5)\"},{\"column_name\":\"1st Half Team 1 Total Goals\",\"value\":\"Over (0.5),Over (1),Under (1),Under (1.5)\"},{\"column_name\":\"1st Half Team 2 Total Goals\",\"value\":\"Under (0.5)\"},{\"column_name\":\"1st Half Goals Handicap\",\"value\":\"SCG Muangthong United (0.5),SCG Muangthong United (0),SCG Muangthong United (-0.5),SCG Muangthong United (-1),Port FC (1),Port FC (1.5)\"},{\"column_name\":\"1st Half Correct Score\",\"value\":\"1-0\"},{\"column_name\":\"Highest Scoring Half\",\"value\":\"1st < 2nd\"},{\"column_name\":\"2nd Half Result\",\"value\":\"W2\"},{\"column_name\":\"1st Half Goals Handicap 3 Way\",\"value\":\"SCG Muangthong United (0),Tie Port FC (1)\"},{\"column_name\":\"Goals Asian Handicap\",\"value\":\"Port FC (-1.25),SCG Muangthong United (1.25),SCG Muangthong United (0.75),Port FC (-0.75),Port FC (-0.25),Port FC (0.25),Port FC (0.75),Port FC (1.25),Port FC (1.75)\"},{\"column_name\":\"Total Goals Asian\",\"value\":\"Over (1.25),Over (1.75),Over (2.25),Over (2.75),Under (2.75),Over (3.25),Under (3.25),Under (3.75)\"},{\"column_name\":\"2nd Half Total Goals Odd\\/Even\",\"value\":\"Even\"},{\"column_name\":\"Total Goals 3 Way\",\"value\":\"Over (2),Exactly (3),Under (4)\"},{\"column_name\":\"1st Half Total Goals 3 Way\",\"value\":\"Exactly (1),Under (2)\"},{\"column_name\":\"1st Half Goals Asian Handicap\",\"value\":\"SCG Muangthong United (0.25),SCG Muangthong United (-0.25),Port FC (0.75),SCG Muangthong United (-0.75),SCG Muangthong United (-1.25),Port FC (1.25)\"},{\"column_name\":\"1st Half Total Goals Asian\",\"value\":\"Under (0.75),Over (0.75),Under (1.25),Over (1.25),Under (1.75)\"},{\"column_name\":\"Rest Of The Match (Current Score: 1-0)\",\"value\":\"W2\"}]}'),
(6, '16022859', 1, '{\"uid\":1,\"game_id\":16022859,\"sport_id\":1,\"tableData\":[{\"column_name\":\"Match Result\",\"value\":\"Draw\"},{\"column_name\":\"Double Chance\",\"value\":\"2 or Draw,1 or Draw\"},{\"column_name\":\"Total Goals\",\"value\":\"Over (1.5),Over (2),Over (2.5),Over (3),Over (3.5),Over (4),Under (4),Under (4.5),Under (5),Under (5.5),Under (6),Under (6.5)\"},{\"column_name\":\"Team 1 Total Goals\",\"value\":\"Over (0.5),Over (1),Over (1.5),Over (2),Under (2),Under (2.5),Under (3)\"},{\"column_name\":\"Team 2 Total Goals\",\"value\":\"Over (0.5),Over (1),Over (1.5),Under (2),Over (2),Under (2.5),Under (3)\"},{\"column_name\":\"Goals Handicap\",\"value\":\"KF Drenasi (1.5),KF Drenasi (1),KF Drenasi (0.5),Dukagjini FK (0),KF Drenasi (0),Dukagjini FK (0.5),Dukagjini FK (1),Dukagjini FK (1.5),Dukagjini FK (2),Dukagjini FK (2.5)\"},{\"column_name\":\"Goals Handicap 3 Way\",\"value\":\"KF Drenasi (1),Tie: Dukagjini FK (0),Dukagjini FK (1),Dukagjini FK (2)\"},{\"column_name\":\"2nd Goal To Score\",\"value\":\"Team 2\"},{\"column_name\":\"3rd Goal To Score\",\"value\":\"Team 2\"},{\"column_name\":\"4th Goal To Score\",\"value\":\"Team 1\"},{\"column_name\":\"5th Goal To Score\",\"value\":\"No Goal\"},{\"column_name\":\"Draw No Bet\",\"value\":\"Team 2,Team 1\"},{\"column_name\":\"Both Teams To Score\",\"value\":\"Yes\"},{\"column_name\":\"Total Goals Odd\\/Even\",\"value\":\"Even\"},{\"column_name\":\"Correct Score\",\"value\":\"2-2\"},{\"column_name\":\"1st Half Result\",\"value\":\"W1\"},{\"column_name\":\"1st Half Double Chance\",\"value\":\"12,1X\"},{\"column_name\":\"1st Half Total Goals\",\"value\":\"Over (0.5),Under (1),Over (1),Under (1.5),Under (2),Under (2.5)\"},{\"column_name\":\"1st Half Team 1 Total Goals\",\"value\":\"Over (0.5),Under (1),Over (1),Under (1.5)\"},{\"column_name\":\"1st Half Team 2 Total Goals\",\"value\":\"Under (0.5),Under (1)\"},{\"column_name\":\"1st Half Goals Handicap\",\"value\":\"KF Drenasi (0.5),KF Drenasi (0),KF Drenasi (-0.5),Dukagjini FK (1),KF Drenasi (-1),Dukagjini FK (1.5)\"},{\"column_name\":\"1st Half Correct Score\",\"value\":\"1-0\"},{\"column_name\":\"Highest Scoring Half\",\"value\":\"1st < 2nd\"},{\"column_name\":\"2nd Half Result\",\"value\":\"W2\"},{\"column_name\":\"1st Half Goals Handicap 3 Way\",\"value\":\"KF Drenasi (0),Tie Dukagjini FK (1)\"},{\"column_name\":\"Goals Asian Handicap\",\"value\":\"KF Drenasi (1.75),KF Drenasi (1.25),KF Drenasi (0.75),KF Drenasi (0.25),Dukagjini FK (-0.25),KF Drenasi (-0.25),Dukagjini FK (0.25),Dukagjini FK (0.75),Dukagjini FK (1.25),Dukagjini FK (1.75),Dukagjini FK (2.25)\"},{\"column_name\":\"Total Goals Asian\",\"value\":\"Over (1.75),Over (2.25),Over (2.75),Over (3.25),Over (3.75),Under (3.75),Under (4.25),Over (4.25),Under (4.75),Under (5.25),Under (5.75)\"},{\"column_name\":\"Total Goals 3 Way\",\"value\":\"Over (2),Over (3),Exactly (4),Under (5),Under (6)\"},{\"column_name\":\"1st Half Total Goals 3 Way\",\"value\":\"Exactly (1),Under (2)\"},{\"column_name\":\"1st Half Goals Asian Handicap\",\"value\":\"KF Drenasi (0.25),KF Drenasi (-0.25),Dukagjini FK (0.75),KF Drenasi (-0.75),KF Drenasi (-1.25),Dukagjini FK (1.25)\"},{\"column_name\":\"1st Half Total Goals Asian\",\"value\":\"Under (0.75),Over (0.75),Under (1.25),Over (1.25),Under (1.75),Under (2.25)\"},{\"column_name\":\"Rest Of The Match (Current Score: 1-0)\",\"value\":\"W2\"},{\"column_name\":\"Rest Of The Match (Current Score: 1-2)\",\"value\":\"W1\"}]}');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `unique_id` int(100) NOT NULL,
  `username` varchar(100) NOT NULL,
  `name` varchar(300) NOT NULL,
  `first_name` varchar(100) NOT NULL,
  `last_name` varchar(100) NOT NULL,
  `middle_name` varchar(100) NOT NULL,
  `gender` varchar(1) NOT NULL,
  `email` varchar(100) NOT NULL,
  `phone` varchar(20) NOT NULL,
  `password` varchar(100) NOT NULL,
  `balance` double DEFAULT '0',
  `reg_info_incomplete` varchar(500) NOT NULL,
  `address` varchar(500) NOT NULL,
  `reg_date` date NOT NULL,
  `birth_date` date NOT NULL,
  `doc_number` varchar(100) NOT NULL,
  `currency_id` int(11) NOT NULL DEFAULT '1',
  `casino_balance` double NOT NULL DEFAULT '0',
  `exclude_date` date NOT NULL,
  `bonus_id` int(11) NOT NULL,
  `games` varchar(100) NOT NULL,
  `super_bet` varchar(200) NOT NULL,
  `country_code` varchar(100) NOT NULL,
  `doc_issued_by` varchar(200) NOT NULL,
  `doc_issue_date` date NOT NULL,
  `doc_issue_code` varchar(100) NOT NULL,
  `province` varchar(100) NOT NULL,
  `iban` varchar(100) NOT NULL,
  `active_step` varchar(100) NOT NULL,
  `active_step_state` varchar(100) NOT NULL,
  `subscribed_to_news` tinyint(1) NOT NULL,
  `bonus_balance` double NOT NULL,
  `frozen_balance` double NOT NULL,
  `bonus_win_balance` double NOT NULL,
  `city` varchar(200) NOT NULL,
  `has_free_bets` tinyint(1) NOT NULL,
  `loyalty_point` int(100) NOT NULL,
  `loyalty_earned_points` int(100) NOT NULL,
  `loyalty_exchanged_points` int(100) NOT NULL,
  `loyalty_level_id` int(100) NOT NULL,
  `affiliate_id` int(100) NOT NULL,
  `is_verified` tinyint(1) NOT NULL,
  `incorrect_fields` int(100) NOT NULL,
  `loyalty_point_usage_period` int(100) NOT NULL,
  `loyalty_min_exchange_point` int(100) NOT NULL,
  `loyalty_max_exchange_point` int(100) NOT NULL,
  `active_time_in_casino` int(100) NOT NULL,
  `last_read_message` varchar(200) NOT NULL,
  `unread_count` int(100) NOT NULL,
  `last_login_date` date NOT NULL,
  `swift_code` varchar(100) NOT NULL,
  `counter_offer_min_amount` int(100) NOT NULL,
  `sportsbook_profile_id` int(100) NOT NULL,
  `subscribe_to_email` tinyint(1) NOT NULL,
  `subscribe_to_sms` tinyint(1) NOT NULL,
  `subscribe_to_bonus` tinyint(1) NOT NULL,
  `is_tax_applicable` tinyint(1) NOT NULL,
  `nem_id_token` varchar(100) NOT NULL,
  `mobile_phone` varchar(100) NOT NULL,
  `personal_id` int(100) NOT NULL,
  `zip_code` varchar(100) NOT NULL,
  `additional_address` varchar(300) NOT NULL,
  `btag` varchar(100) NOT NULL,
  `birth_region` varchar(100) NOT NULL,
  `supported_currencies` varchar(100) NOT NULL,
  `wallets` varchar(100) NOT NULL,
  `previous_login_time` date NOT NULL,
  `nick_name` varchar(100) NOT NULL,
  `is_super_bet_available` varchar(100) NOT NULL,
  `is_gdpr_passed` varchar(100) NOT NULL,
  `subscribe_to_internal_message` varchar(100) NOT NULL,
  `subscribe_to_push_notification` tinyint(1) NOT NULL,
  `subscribe_to_phone_call` tinyint(1) NOT NULL,
  `title` varchar(100) NOT NULL,
  `terms_and_conditions_version` varchar(100) NOT NULL,
  `terms_and_conditions_acceptance_date` varchar(100) NOT NULL,
  `last_preferred_currency` varchar(100) NOT NULL,
  `is_agent` tinyint(1) NOT NULL,
  `is_two_factor_authentication_enabled` int(100) NOT NULL,
  `authentication_status` int(100) NOT NULL,
  `qr_code_origin` varchar(100) NOT NULL,
  `session_duration` varchar(100) NOT NULL,
  `is_new_client` varchar(100) NOT NULL,
  `unplayed_balance` varchar(100) NOT NULL,
  `language` varchar(100) NOT NULL,
  `casino_unplayed_balance` varchar(100) NOT NULL,
  `client_notifications` varchar(100) NOT NULL,
  `bonus_money` varchar(100) NOT NULL,
  `loyalty_last_earned_points` varchar(100) NOT NULL,
  `is_cash_out_available` tinyint(1) NOT NULL,
  `sur_name` varchar(100) NOT NULL,
  `full_name` varchar(300) NOT NULL,
  `calculatedBalance` int(11) NOT NULL DEFAULT '0',
  `calculatedBonus` int(11) NOT NULL DEFAULT '0',
  `skype_request` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `unique_id`, `username`, `name`, `first_name`, `last_name`, `middle_name`, `gender`, `email`, `phone`, `password`, `balance`, `reg_info_incomplete`, `address`, `reg_date`, `birth_date`, `doc_number`, `currency_id`, `casino_balance`, `exclude_date`, `bonus_id`, `games`, `super_bet`, `country_code`, `doc_issued_by`, `doc_issue_date`, `doc_issue_code`, `province`, `iban`, `active_step`, `active_step_state`, `subscribed_to_news`, `bonus_balance`, `frozen_balance`, `bonus_win_balance`, `city`, `has_free_bets`, `loyalty_point`, `loyalty_earned_points`, `loyalty_exchanged_points`, `loyalty_level_id`, `affiliate_id`, `is_verified`, `incorrect_fields`, `loyalty_point_usage_period`, `loyalty_min_exchange_point`, `loyalty_max_exchange_point`, `active_time_in_casino`, `last_read_message`, `unread_count`, `last_login_date`, `swift_code`, `counter_offer_min_amount`, `sportsbook_profile_id`, `subscribe_to_email`, `subscribe_to_sms`, `subscribe_to_bonus`, `is_tax_applicable`, `nem_id_token`, `mobile_phone`, `personal_id`, `zip_code`, `additional_address`, `btag`, `birth_region`, `supported_currencies`, `wallets`, `previous_login_time`, `nick_name`, `is_super_bet_available`, `is_gdpr_passed`, `subscribe_to_internal_message`, `subscribe_to_push_notification`, `subscribe_to_phone_call`, `title`, `terms_and_conditions_version`, `terms_and_conditions_acceptance_date`, `last_preferred_currency`, `is_agent`, `is_two_factor_authentication_enabled`, `authentication_status`, `qr_code_origin`, `session_duration`, `is_new_client`, `unplayed_balance`, `language`, `casino_unplayed_balance`, `client_notifications`, `bonus_money`, `loyalty_last_earned_points`, `is_cash_out_available`, `sur_name`, `full_name`, `calculatedBalance`, `calculatedBonus`, `skype_request`) VALUES
(1, 0, 'mahesh', '', 'MAHESH', 'JALODARA', '', '', 'mahesh@gmail.com', '8866825513', '123456', 149987, '', '', '0000-00-00', '0000-00-00', '', 1, 0, '0000-00-00', 0, '', '', '', '', '0000-00-00', '', '', '', '', '', 0, 0, 0, 0, '', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 0, '0000-00-00', '', 0, 0, 0, 0, 0, 0, '', '', 0, '', '', '', '', '', '', '0000-00-00', '', '', '', '', 0, 0, '', '', '', '', 0, 0, 0, '', '', '', '', '', '', '', '', '', 0, '', '', 0, 0, 0),
(2, 32232, 'mj', '', 'Mj', 'Mj', '', '', 'mj@gmail.com', '911234567890', '123456', 54, '', '', '0000-00-00', '0000-00-00', '', 1, 44, '0000-00-00', 0, '', '', '', '', '0000-00-00', '', '', '', '', '', 0, 0, 0, 0, '', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 0, '0000-00-00', '', 0, 0, 0, 0, 0, 0, '', '', 0, '', '', '', '', '', '', '0000-00-00', '', '', '', '', 0, 0, '', '', '', '', 0, 0, 0, '', '', '', '', '', '', '', '', '', 0, '', '', 20, 0, 0),
(3, 4323124, 'pradeep', '', 'Pradeep', 'Thite', '', '', 'pradeep@gmail.com', '911234567899', '123456', 56, '', '', '0000-00-00', '0000-00-00', '', 1, 0, '0000-00-00', 0, '', '', '', '', '0000-00-00', '', '', '', '', '', 0, 0, 0, 0, '', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 0, '0000-00-00', '', 0, 0, 0, 0, 0, 0, '', '', 0, '', '', '', '', '', '', '0000-00-00', '', '', '', '', 0, 0, '', '', '', '', 0, 0, 0, '', '', '', '', '', '', '', '', '', 0, '', '', 46, 0, 0);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `bets_tmp`
--
ALTER TABLE `bets_tmp`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `currency`
--
ALTER TABLE `currency`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `results`
--
ALTER TABLE `results`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `bets_tmp`
--
ALTER TABLE `bets_tmp`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `currency`
--
ALTER TABLE `currency`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `results`
--
ALTER TABLE `results`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

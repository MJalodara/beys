<?php
include 'config.php';
// echo "come to login.php";
header("Content-Type: application/json");
header('Access-Control-Allow-Origin: *');


if (isset($_SERVER['HTTP_ORIGIN'])) {
    header("Access-Control-Allow-Origin: {$_SERVER['HTTP_ORIGIN']}");
    header('Access-Control-Allow-Credentials: true');
    header('Access-Control-Max-Age: 86400');    // cache for 1 day
}
if ($_SERVER['REQUEST_METHOD'] == 'OPTIONS') {

    if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_METHOD']))
        header("Access-Control-Allow-Methods: POST, POST, OPTIONS");

    if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']))
        header("Access-Control-Allow-Headers: {$_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']}");

    exit(0);
}

$res = file_get_contents('php://input');
// $password = json_decode($_POST['password']);
$json = json_decode($res);
$username =  $json->username;
$password = $json->password;
$sel = mysqli_query($con, "select u.*,c.currency from users u LEFT JOIN currency c ON u.currency_id=c.id  where u.username ='" . $username . "'");
$data = array();

// while ($row = mysqli_fetch_array($sel)) {
//     $data[] = array("email" => $row['email'], "password" => $row['password'],"name" => $row['first_name'], "username"=>$row['username'], "lastname"=>$row['last_name'], "id"=>$row['id'], "phone"=>$row['phone'],"balance"=>$row['balance'],"currency"=>$row['currency']);
// }
$original_data = array();
$mydata = array();
while ($row = mysqli_fetch_array($sel, MYSQLI_ASSOC)) {
    $mydata[] = $row;


    $mydata[0]['id'] = (int) $mydata[0]['id'];
    $mydata[0]['unique_id'] = (int) $mydata[0]['unique_id'];
    $mydata[0]['balance'] = (float) $mydata[0]['balance'];
    $mydata[0]['casino_balance'] = (float) $mydata[0]['casino_balance'];
    $mydata[0]['bonus_id'] = (int) $mydata[0]['bonus_id'];
    $mydata[0]['bonus_balance'] = (float) $mydata[0]['bonus_balance'];
    $mydata[0]['frozen_balance'] = (float) $mydata[0]['frozen_balance'];
    $mydata[0]['has_free_bets'] = (int) $mydata[0]['has_free_bets'];
    $mydata[0]['loyalty_point'] = (int) $mydata[0]['loyalty_point'];
    $mydata[0]['loyalty_earned_points'] = (int) $mydata[0]['loyalty_earned_points'];
    $mydata[0]['loyalty_exchanged_points'] = (int) $mydata[0]['loyalty_exchanged_points'];
    $mydata[0]['loyalty_level_id'] = (int) $mydata[0]['loyalty_level_id'];
    $mydata[0]['affiliate_id'] = (int) $mydata[0]['affiliate_id'];
    $mydata[0]['is_verified'] = (int) $mydata[0]['is_verified'];
    $mydata[0]['incorrect_fields'] = (int) $mydata[0]['incorrect_fields'];
    $mydata[0]['loyalty_point_usage_period'] = (int) $mydata[0]['loyalty_point_usage_period'];
    $mydata[0]['loyalty_min_exchange_point'] = (int) $mydata[0]['loyalty_min_exchange_point'];
    $mydata[0]['loyalty_max_exchange_point'] = (int) $mydata[0]['loyalty_max_exchange_point'];
    $mydata[0]['active_time_in_casino'] = (int) $mydata[0]['active_time_in_casino'];
    $mydata[0]['unread_count'] = (int) $mydata[0]['unread_count'];
    $mydata[0]['counter_offer_min_amount'] = (int) $mydata[0]['counter_offer_min_amount'];
    $mydata[0]['sportsbook_profile_id'] = (int) $mydata[0]['sportsbook_profile_id'];
    $mydata[0]['subscribe_to_email'] = (int) $mydata[0]['subscribe_to_email'];
    $mydata[0]['subscribe_to_sms'] = (int) $mydata[0]['subscribe_to_sms'];
    $mydata[0]['subscribe_to_bonus'] = (int) $mydata[0]['subscribe_to_bonus'];
    $mydata[0]['is_tax_applicable'] = (int) $mydata[0]['is_tax_applicable'];
    $mydata[0]['personal_id'] = (int) $mydata[0]['personal_id'];
    $mydata[0]['subscribe_to_push_notification'] = (int) $mydata[0]['subscribe_to_push_notification'];
    $mydata[0]['subscribe_to_phone_call'] = (int) $mydata[0]['subscribe_to_phone_call'];
    $mydata[0]['is_agent'] = (int) $mydata[0]['is_agent'];
    $mydata[0]['is_two_factor_authentication_enabled'] = (int) $mydata[0]['is_two_factor_authentication_enabled'];
    $mydata[0]['authentication_status'] = (int) $mydata[0]['authentication_status'];
    $mydata[0]['is_cash_out_available'] = (int) $mydata[0]['is_cash_out_available'];
    $mydata[0]['calculatedBalance'] = (int) $mydata[0]['calculatedBalance'];
    $mydata[0]['calculatedBonus'] = (int) $mydata[0]['calculatedBonus'];
    $mydata[0]['skype_request'] = (int) $mydata[0]['skype_request'];
    $mydata[0]['bonus_win_balance'] = (int) $mydata[0]['bonus_win_balance'];
    $mydata[0]['currency_name'] = $mydata[0]['currency'];
    array_push($data, $mydata);
}

    $pswd = $mydata[0]['password'];
    if ($password == $pswd) {

        $data['success'] = "true";

        echo json_encode($data);
    } else {
        echo "false";
    }
    // echo json_encode($data[0]);

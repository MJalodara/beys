<?php
// include 'config.php';
include 'db.php';
include 'functions.php';
db_connect();


function from_obj(&$type, $default = "")
{
  return isset($type) ? $type : $default;
}

$res = file_get_contents('php://input');
$json = json_decode($res);
$gameData = from_obj($json->gameData, array());
foreach ($gameData as $row) {
  if ($row && $row->id) {
  $queryU = $con->prepare("SELECT *
      FROM `bet_slip_tmp`
      WHERE game_id=:game_id
      ");
  $queryU->bindParam(":game_id", $row->id);
  $queryU->execute();
  $userData = array();
  if ($queryU->rowCount() > 0) {
    $paramts = array(
      "info" => json_encode($row->info),
    );
    $id = updateRow("bet_slip_tmp", $paramts, array("game_id" => $row->id));
  }
  }
}

$response = array();
$response['success'] = "true";
$response['result'] = "true";
$response['result_text'] = null;
echo json_encode($response);

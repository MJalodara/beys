<?php
include '../db.php';
include '../functions.php';
db_connect();

header("Content-Type: application/json");
header('Access-Control-Allow-Origin: *');


if (isset($_SERVER['HTTP_ORIGIN'])) {
    header("Access-Control-Allow-Origin: {$_SERVER['HTTP_ORIGIN']}");
    header('Access-Control-Allow-Credentials: true');
    header('Access-Control-Max-Age: 86400');    // cache for 1 day
}
if ($_SERVER['REQUEST_METHOD'] == 'OPTIONS') {

    if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_METHOD']))
        header("Access-Control-Allow-Methods: POST, POST, OPTIONS");

    if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']))
        header("Access-Control-Allow-Headers: {$_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']}");

    exit(0);
}

function from_obj(&$type,$default = "") {
    return isset($type)? $type : $default;
}

$res = file_get_contents('php://input');
$json = json_decode($res);
$sport_id =  $json->sport_id;
$game_id =  $json->game_id;
$tableData =  $json->tableData;

// find game id 
  $gameResultData = $con->prepare ("SELECT  *
  FROM `results` 
  WHERE   game_id=:game_id");
  $gameResultData->bindParam ( ":game_id", $game_id);
  $gameResultData->execute ();

  if ($gameResultData->rowCount () == 1) {
  $updateParams=array(
  'json_lines'=>json_encode($json),
  );
  $uid=updateRow("results",$updateParams,array("game_id"=>$game_id));

  }else{
  $paramts=array(
  'game_id'=>$game_id,
  'sport_id'=>$sport_id,
  'json_lines'=>json_encode($json),
  );
  $id=insertRow("results",$paramts);
  }


$response = array();


$response['success'] = "true";
$response['result'] = "true";
$response['result_text']=null;
echo json_encode($response);

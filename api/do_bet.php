<?php
// include 'config.php';
include '../db.php';
include '../functions.php';
db_connect();


header("Content-Type: application/json");
header('Access-Control-Allow-Origin: *');


if (isset($_SERVER['HTTP_ORIGIN'])) {
    header("Access-Control-Allow-Origin: {$_SERVER['HTTP_ORIGIN']}");
    header('Access-Control-Allow-Credentials: true');
    header('Access-Control-Max-Age: 86400');    // cache for 1 day
}
if ($_SERVER['REQUEST_METHOD'] == 'OPTIONS') {

    if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_METHOD']))
        header("Access-Control-Allow-Methods: POST, POST, OPTIONS");

    if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']))
        header("Access-Control-Allow-Headers: {$_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']}");

    exit(0);
}    

function from_obj(&$type,$default = "") {
    return isset($type)? $type : $default;
}

$res = file_get_contents('php://input');
$json = json_decode($res);

$bet_type_id = from_obj( $json->type, "");
$bet_type_name = from_obj( $json->bet_type_name, "");
$source = from_obj( $json->source, "");
$is_offer = from_obj( $json->is_offer, "");
$mode = from_obj( $json->mode, "");
$each_way = from_obj( $json->each_way, "");
$bets = from_obj( $json->events, Array());
$amount = from_obj( $json->amount, "0");
$odd_type = from_obj( $json->odd_type, "");
$currency = from_obj( $json->currency, "");
$is_live = from_obj( $json->is_live, "");
$payout = from_obj( $json->payout, "");
$possible_win = from_obj( $json->possible_win, "");
$accept_type_id = from_obj( $json->accept_type_id, "");
$system_min_count = from_obj( $json->system_min_count, "");
$client_login = from_obj( $json->client_login, "");
$barcode = from_obj( $json->barcode, "");
$calc_date = from_obj( $json->calc_date, "");
$date_time = from_obj( $json->date_time, "");
$client_bonus_id = from_obj( $json->client_bonus_id, "");
$is_super_bet = from_obj( $json->is_super_bet, "");
$is_bonus_money = from_obj( $json->is_bonus_money, "");
$draw_number = from_obj( $json->draw_number, "");
$tags = from_obj( $json->tags, "");
$old_bet_id = from_obj( $json->amount, "");
$auto_cash_out_amount = from_obj( $json->auto_cash_out_amount, "");
$additional_amount = from_obj( $json->additional_amount, "");
$total_partial_cashout_amount = from_obj( $json->total_partial_cashout_amount, "");
$remaining_stake = from_obj( $json->remaining_stake, "");
$tax_amount = from_obj( $json->tax_amount, "");
$additional_info = from_obj( $json->additional_info, "");
$has_recalculation_reason = from_obj( $json->has_recalculation_reason, "");
$info_cashdesk_id = from_obj( $json->info_cashdesk_id, "");
$stake_tax_amount = from_obj( $json->stake_tax_amount, "");
$uid = from_obj( $json->uid, "");
$response = array();

        $x=0;
        foreach($bets as $cbets)
        {
            if($x==0)
            {
                $farr = $cbets->selection_price;
                $x++;
            }
            else
            {
                $farr *= $cbets->selection_price;
            }
        }

        $k= $farr;
    
    $queryUser = $con->prepare ( "SELECT balance
        FROM `users`
        WHERE id=:uid
        ");
    $queryUser->bindParam(":uid", $uid);
    $queryUser->execute ();
    $userDataTemp = array ();
    if ($queryUser->rowCount () > 0) {
        $userDataTemp= $queryUser->fetch ( PDO::FETCH_ASSOC );
        $balanace_after = (float)$userDataTemp['balance'] - (float)$amount;
    }
if((float)$userDataTemp['balance'] >=(float)$amount){
    $bets_paramts=array(
        "type"=>$bet_type_id,
        "uid"=>$uid,
        "source"=>$source,
        "currency"=>$currency,
        "is_live"=>$is_live,
        "payout"=>$payout,
        "possible_win"=>$possible_win,
        "accept_type_id"=>$accept_type_id,
        "system_min_count"=>$system_min_count,
        "client_login"=>$client_login,
        "barcode"=>$barcode,
        "calc_date"=>$calc_date,
        "date_time"=>$date_time,
        "client_bonus_id"=>$client_bonus_id,
        "is_super_bet"=>$is_super_bet,
        "is_bonus_money"=>$is_bonus_money,
        "draw_number"=>$draw_number,
        "tags"=>$tags,
        "old_bet_id"=>$old_bet_id,
        "auto_cash_out_amount"=>$auto_cash_out_amount,
        "additional_amount"=>$additional_amount,
        "total_partial_cashout_amount"=>$total_partial_cashout_amount,
        "remaining_stake"=>$remaining_stake,
        "tax_amount"=>$tax_amount,
        "additional_info"=>$additional_info,
        "has_recalculation_reason"=>$has_recalculation_reason,
        "info_cashdesk_id"=>$info_cashdesk_id,
        "stake_tax_amount"=>$stake_tax_amount,
        "each_way"=>$each_way,
        "k"=>$k,      
        "balanace_after"=>$balanace_after,      
        "amount"=>$amount,      
        "odd_type"=>$odd_type,
    );
    $bets_slip_id=insertRow("bets_tmp",$bets_paramts);



    
    foreach ($bets as $bet) 
    {
        // $event_id = $bet->event_id;
        $selection_id = from_obj( $bet->selection_id, 0);
        $coeficient = from_obj( $bet->coeficient, "");
        $outcome = from_obj( $bet->outcome, 0);
        $outcome_name =from_obj(  $bet->outcome_name, "");
        $game_info = from_obj( $bet->game_info, "");
        $event_name = from_obj( $bet->event_name, "");
        $game_start_date =from_obj(  $bet->game_start_date, "");
        $team1 = from_obj( $bet->team1, "");
        $team2 = from_obj( $bet->team2, "");
        $competition_name = from_obj( $bet->competition_name, "");
        $game_id = from_obj( $bet->game_id, "");
        $is_live = from_obj( $bet->is_live, false);
        $sport_id = from_obj( $bet->sport_id, "");
        $sport_name = from_obj( $bet->sport_name, "");
        $sport_index =from_obj(  $bet->sport_index, "");
        $region_name = from_obj( $bet->region_name, "");
        $market_name =from_obj(  $bet->market_name, "");
        $match_display_id = from_obj( $bet->match_display_id, "");
        $game_name = from_obj( $bet->game_name, "");
        $basis = from_obj( $bet->basis, "");
        $match_info = from_obj( $bet->match_info, "");
        $exact_name = from_obj( $bet->exact_name, "");
        //$info =from_obj(  $bet->info, "{}");
        $home_score = from_obj( $bet->home_score, 0);
        $away_score = from_obj( $bet->away_score, 0);
        $cash_out_price =from_obj(  $bet->cash_out_price, "0");
        $selection_price = from_obj( $bet->selection_price, "0");
        $maxBet = from_obj( $bet->maxBet, "0");
       
        if($is_live === true  || $is_live === 'true'){
            $is_live = '1';
        }else{
            $is_live = '0';
        }
        $bet_arra_paramts=array(
            "bet_id"=>$bets_slip_id,
            "selection_id"=>$selection_id,
            "coeficient"=>$coeficient,
            "outcome"=>$outcome,
            "outcome_name"=>$outcome_name,
            "game_info"=>$game_info,
            "event_name"=>$event_name,
            "game_start_date"=>$game_start_date,
            "team1"=>$team1,
            "team2"=>$team2,
            "competition_name"=>$competition_name,
            "game_id"=>$game_id,
            "is_live"=>$is_live,
            "sport_id"=>$sport_id,
            "sport_name"=>$sport_name,
            "sport_index"=>$sport_index,
            "region_name"=>$region_name,
            "market_name"=>$market_name,
            "match_display_id"=>$match_display_id,
            "game_name"=>$game_name,
            "basis"=>$basis,
            "match_info"=>$match_info,
            "exact_name"=>$exact_name,
        //    "info"=>$info,
            "home_score"=>$home_score,
            "away_score"=>$away_score,
            "cash_out_price"=>$cash_out_price,
            "selection_price"=>$selection_price,
            "maxBetAmount"=> (double)$maxBet,
        );
        $sbets_slip_id=insertRow("bet_slip_tmp",$bet_arra_paramts);
      }

if($bets_slip_id){
    
    $queryU = $con->prepare ( "SELECT balance
    FROM `users`
    WHERE id=:uid
    " );
    $queryU->bindParam(":uid", $uid);
    $queryU->execute ();
    $userData = array ();
    if ($queryU->rowCount () > 0) {
    $userData= $queryU->fetch ( PDO::FETCH_ASSOC );


        $totalAmout = (float)$userData['balance'] - (float)$amount;
        $paramts=array(
            "balance"=>$totalAmout,
            );
            $id=updateRow("users",$paramts,array("id"=>$uid));
        
        }
    $query = $con->prepare ( "SELECT bt.id,bt.amount,bt.type ,bt.k,bt.bonus_bet_amount,bt.is_live
            FROM `bets_tmp` bt
            WHERE bt.id=:id
                " );
    $query->bindParam(":id", $bets_slip_id);

    $query->execute ();
        $batsData = array ();
        if ($query->rowCount () > 0) {
        $batsData= $query->fetch ( PDO::FETCH_ASSOC );

        $queryE = $con->prepare ( "SELECT selection_id,game_id,competition_name  competition, home_score home_team,away_score away_team, game_start_date start_date,market_name market_name,event_name event_name,info m_arjel_code 
            FROM `bet_slip_tmp`
            WHERE bet_id=:id
                " );
        $queryE->bindParam(":id", $bets_slip_id);
    $queryE->execute ();
        $eventData = array ();
        if ($queryE->rowCount () > 0) {
            $eventData= $queryE->fetchAll ( PDO::FETCH_ASSOC );
        $batsData['events'] = $eventData;
        $batsData['is_superbet'] = false;
        $batsData['FreeBetAmount'] = 0.0;
        if($batsData['is_live'] === '1'){
            $batsData['IsLive'] = true;
        }else{
            $batsData['IsLive'] = false;
        }
    
        
        $response['success'] = "OK";
        $response['result'] = "OK";
        $response['result_text']=null;
        $response['details'] = $batsData;
        echo json_encode($response);  
        }
        
    }
} else {
        echo "false";
}

}else{
    $response['success'] = "NO";
    $response['result'] = "OK";
    $response['result_text']='Balance not valid';
    $response['details'] = [];
    echo json_encode($response);  
}
<?php
include '../db.php';
include '../functions.php';
db_connect();

header("Content-Type: application/json");
header('Access-Control-Allow-Origin: *');


if (isset($_SERVER['HTTP_ORIGIN'])) {
    header("Access-Control-Allow-Origin: {$_SERVER['HTTP_ORIGIN']}");
    header('Access-Control-Allow-Credentials: true');
    header('Access-Control-Max-Age: 86400');    // cache for 1 day
}
if ($_SERVER['REQUEST_METHOD'] == 'OPTIONS') {

    if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_METHOD']))
        header("Access-Control-Allow-Methods: POST, POST, OPTIONS");

    if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']))
        header("Access-Control-Allow-Headers: {$_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']}");

    exit(0);
}

$res = file_get_contents('php://input');
// $password = json_decode($_POST['password']);
$json = json_decode($res);
$uid =  $json->uid;
$data = array();
$mydata = array();

$query = $con->prepare("select u.*,c.currency from users u LEFT JOIN currency c ON u.currency_id=c.id  where u.id =:uid");

$query->bindParam(":uid", $uid);

$query->execute();
$userData = array();

if ($query->rowCount() > 0) {
    $userData = $query->fetch(PDO::FETCH_ASSOC);
    $mydata[0]['id'] = (int) $userData['id'];
    $mydata[0]['unique_id'] = (int) $userData['unique_id'];
    $mydata[0]['username'] = $userData['username'];
    $mydata[0]['name'] = $userData['name'];
    $mydata[0]['first_name'] = $userData['first_name'];
    $mydata[0]['last_name'] = $userData['last_name'];
    $mydata[0]['middle_name'] = $userData['middle_name'];
    $mydata[0]['email'] = $userData['email'];
    $mydata[0]['phone'] = $userData['phone'];
    $mydata[0]['address'] = $userData['address'];
    $mydata[0]['birth_date'] = $userData['birth_date'];
    $mydata[0]['games'] = $userData['games'];
    $mydata[0]['super_bet'] = $userData['super_bet'];
    $mydata[0]['country_code'] = $userData['country_code'];
    $mydata[0]['doc_issued_by'] = $userData['doc_issued_by'];
    $mydata[0]['doc_issue_date'] = $userData['doc_issue_date'];
    $mydata[0]['doc_issue_code'] = $userData['doc_issue_code'];
    $mydata[0]['province'] = $userData['province'];
    $mydata[0]['city'] = $userData['city'];
    $mydata[0]['swift_code'] = $userData['swift_code'];
    $mydata[0]['zip_code'] = $userData['zip_code'];
    $mydata[0]['wallets'] = $userData['wallets'];
    $mydata[0]['title'] = $userData['title'];
    $mydata[0]['language'] = $userData['language'];
    $mydata[0]['balance'] = (float) $userData['balance'];
    $mydata[0]['casino_balance'] = (float) $userData['casino_balance'];
    $mydata[0]['bonus_id'] = (int) $userData['bonus_id'];
    $mydata[0]['bonus_balance'] = (float) $userData['bonus_balance'];
    $mydata[0]['frozen_balance'] = (float) $userData['frozen_balance'];
    $mydata[0]['has_free_bets'] = (int) $userData['has_free_bets'];
    $mydata[0]['loyalty_point'] = (int) $userData['loyalty_point'];
    $mydata[0]['loyalty_earned_points'] = (int) $userData['loyalty_earned_points'];
    $mydata[0]['loyalty_exchanged_points'] = (int) $userData['loyalty_exchanged_points'];
    $mydata[0]['loyalty_level_id'] = (int) $userData['loyalty_level_id'];
    $mydata[0]['affiliate_id'] = (int) $userData['affiliate_id'];
    $mydata[0]['is_verified'] = (int) $userData['is_verified'];
    $mydata[0]['incorrect_fields'] = (int) $userData['incorrect_fields'];
    $mydata[0]['loyalty_point_usage_period'] = (int) $userData['loyalty_point_usage_period'];
    $mydata[0]['loyalty_min_exchange_point'] = (int) $userData['loyalty_min_exchange_point'];
    $mydata[0]['loyalty_max_exchange_point'] = (int) $userData['loyalty_max_exchange_point'];
    $mydata[0]['active_time_in_casino'] = (int) $userData['active_time_in_casino'];
    $mydata[0]['unread_count'] = (int) $userData['unread_count'];
    $mydata[0]['counter_offer_min_amount'] = (int) $userData['counter_offer_min_amount'];
    $mydata[0]['sportsbook_profile_id'] = (int) $userData['sportsbook_profile_id'];
    $mydata[0]['subscribe_to_email'] = (int) $userData['subscribe_to_email'];
    $mydata[0]['subscribe_to_sms'] = (int) $userData['subscribe_to_sms'];
    $mydata[0]['subscribe_to_bonus'] = (int) $userData['subscribe_to_bonus'];
    $mydata[0]['is_tax_applicable'] = (int) $userData['is_tax_applicable'];
    $mydata[0]['personal_id'] = (int) $userData['personal_id'];
    $mydata[0]['subscribe_to_push_notification'] = (int) $userData['subscribe_to_push_notification'];
    $mydata[0]['subscribe_to_phone_call'] = (int) $userData['subscribe_to_phone_call'];
    $mydata[0]['is_agent'] = (int) $userData['is_agent'];
    $mydata[0]['is_two_factor_authentication_enabled'] = (int) $userData['is_two_factor_authentication_enabled'];
    $mydata[0]['authentication_status'] = (int) $userData['authentication_status'];
    $mydata[0]['is_cash_out_available'] = (int) $userData['is_cash_out_available'];
    $mydata[0]['calculatedBalance'] = (int) $userData['calculatedBalance'];
    $mydata[0]['calculatedBonus'] = (int) $userData['calculatedBonus'];
    $mydata[0]['skype_request'] = (int) $userData['skype_request'];
    $mydata[0]['bonus_win_balance'] = (int) $userData['bonus_win_balance'];
    $mydata[0]['currency_name'] = $userData['currency'];
    // array_push($data, $mydata);

    $data['success'] = "true";
    $data['data'] = $mydata[0];

    echo json_encode($data);
} else {
    $data['success'] = "false";
    echo json_encode($data);
}

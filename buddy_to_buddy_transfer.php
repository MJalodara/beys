<?php
// include 'config.php';
include 'db.php';
include 'functions.php';
db_connect();

function from_obj(&$type,$default = "") {
    return isset($type)? $type : $default;
}

$res = file_get_contents('php://input');
$json = json_decode($res);

$to_userName = from_obj( $json->to_user, "");
$to_user_id = from_obj( $json->to_user_id, "");
$from_user_id = from_obj( $json->from_user_id, "");
$amount = from_obj( $json->amount, "");

$queryFU = $con->prepare ( "SELECT id,balance
    FROM `users`
    WHERE id=:uid
    " );
    $queryFU->bindParam(":uid", $from_user_id);
    $queryFU->execute ();
    $fromUserData = array ();

$queryNew = $con->prepare ( "SELECT id,balance
    FROM `users`
    WHERE (username=:to_userName)
    " );
    $queryNew->bindParam(":to_userName", $to_userName);
    $queryNew->execute ();

    if ($queryFU->rowCount () > 0 && $queryNew->rowCount () > 0) {
    $fromUserData= $queryFU->fetch ( PDO::FETCH_ASSOC );
        $totalAmout = (float)$fromUserData['balance'] - (float)$amount;
        $paramts=array(
            "balance"=>$totalAmout,
            );
            $id=updateRow("users",$paramts,array("id"=>$fromUserData['id']));

             $queryTU = $con->prepare ( "SELECT id,balance
                FROM `users`
                WHERE (username=:to_userName OR id=:uid)
                " );
                $queryTU->bindParam(":to_userName", $to_userName);
                $queryTU->bindParam(":uid", $uid);
                $queryTU->execute ();
                $toUserData = array ();
                if ($queryTU->rowCount () > 0) {
                $toUserData= $queryTU->fetch ( PDO::FETCH_ASSOC );
                    $totalAmoutT = (float)$toUserData['balance'] + (float)$amount;
                    $paramtsT=array(
                        "balance"=>$totalAmoutT,
                        );
                        $id=updateRow("users",$paramtsT,array("id"=>$toUserData['id']));
                    
                }
                $buddy_transfer=array(
                    "tranfer_id"=>rand(),
                    "from_id"=>$fromUserData['id'],
                    "to_id"=>$toUserData['id'],
                    "amount"=>(float)$amount,
                    "date"=>phpNow(),
                );
                $sbets_slip_id=insertRow("buddy_transfer",$buddy_transfer);
        
        $response['success'] = "true";
        $response['result'] = 0;
        echo json_encode($response);  
    }
   
    else{
        $response['success'] = "false";
        $response['result'] = -1;
        echo json_encode($response); 
    }

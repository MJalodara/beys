<?php
// include 'config.php';
include 'db.php';
include 'functions.php';
db_connect();

function from_obj(&$type,$default = "") {
    return isset($type)? $type : $default;
}

$res = file_get_contents('php://input');
$json = json_decode($res);

$uid = from_obj( $json->uid, "");

$response = array();


$query = $con->prepare ( "SELECT *
FROM `bets_tmp`
WHERE uid=:uid" );
$query->bindParam(":uid", $uid);



$query->execute ();
$batsData = array ();
if ($query->rowCount () > 0) {
$batsData= $query->fetchAll ( PDO::FETCH_ASSOC );
$original_data = array();
foreach ($batsData as $bet) 
{
   // $queryE = $con->prepare ( "SELECT selection_id,game_id,competition_name  competition, home_score home_team,away_score away_team, game_start_date start_date,market_name market_name,event_name event_name,info m_arjel_code, team1, team2 
   $queryE = $con->prepare ( "SELECT bet_id,selection_id,coeficient,outcome,outcome_name,game_info,event_name,game_start_date,team1,team2,competition_name,game_id,is_live,sport_id,sport_name,sport_index,region_name,market_name,match_display_id,game_name,basis,match_info,info,home_score,away_score,cash_out_price,selection_price
    FROM `bet_slip_tmp`
    WHERE bet_id=:id
    " );
    $queryE->bindParam(":id", $bet['id']);
    $queryE->execute ();
    $eventData = array ();
    if ($queryE->rowCount () > 0) {
        $eventData= $queryE->fetchAll ( PDO::FETCH_ASSOC );
        $bet['events'] = $eventData;
        $bet['is_superbet'] = false;
        $bet['FreeBetAmount'] = 0.0;
        if($bet['is_live'] === '1'){
        $bet['IsLive'] = true;
        }else{
        $bet['IsLive'] = false;
        }
        array_push($original_data, $bet);
    }
}

$jsonData['bets'] = $original_data;
$response['success'] = "true";
$response['result'] = 0;
$response['data'] = $jsonData; 
echo json_encode($response);  
}else{
    $jsonData['bets'] = [];
    $response['success'] = "true";
    $response['result'] = 0;
    $response['data'] = $jsonData;
    echo json_encode($response); 
}
